package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.ColumnInfo;

import home.crsk.todaysweather.android.dataroom.BaseDTO;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */

public abstract class BaseForecastDTO extends BaseDTO {

    @ColumnInfo(name = "fk_forecast_id")
    public final long fkForecastId;

    public final long time;

    public final int year;

    public final int month;

    public final int day;

    public final int hour;

    public final String icon;

    public BaseForecastDTO(long fkForecastId, long time, int year, int month, int day, int hour, String icon) {
        this.fkForecastId = fkForecastId;
        this.time = time;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.icon = icon;
    }
}
