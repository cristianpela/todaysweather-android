package home.crsk.todaysweather.android.dataroom.search;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import home.crsk.todaysweather.android.dataroom.BaseDTO;

/**
 * .
 * Created by Cristian Pela on 22/1/2018.
 */
@Entity(tableName = "search_history")
public final class SearchHistoryDTO extends BaseDTO{

    public final String search;

    public SearchHistoryDTO(String search) {
        this.search = search;
    }
}
