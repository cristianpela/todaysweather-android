package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import home.crsk.todaysweather.android.dataroom.location.LocationDAO;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
@Dao
public interface ForecastDAO {

    @Insert
    long insertForecast(ForecastDTO entity);

    @Insert
    void insertSimpleForecasts(List<SimpleForecastDTO> entities);

    @Insert
    void insertTextForecast(List<TextForecastDTO> entities);

    @Insert
    void insertHourForecasts(List<HourForecastDTO> entities);

    @Query("SELECT * FROM forecast WHERE fk_location_key=:locationKey " +
            "ORDER BY last_updated DESC LIMIT 1")
    @Transaction
    Maybe<FullForecastDTO> getMostRecentFullForecast(String locationKey);

    @Query("SELECT last_updated FROM forecast WHERE fk_location_key=:locationKey " +
            "ORDER BY last_updated DESC LIMIT 1")
    Single<Long> getLastUpdateTime(String locationKey);

    @Query("DELETE FROM forecast WHERE fk_location_key=:locationKey")
    int deleteForecastsForLocation(String locationKey);



}
