package home.crsk.todaysweather.android.dataroom.search;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import io.reactivex.Maybe;

/**
 * .
 * Created by Cristian Pela on 22/1/2018.
 */
@Dao
public interface SearchHistoryDAO {

    @Query("SELECT search FROM search_history WHERE search=:partialSearch")
    Maybe<String> find(String partialSearch);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void save(SearchHistoryDTO entity);

}