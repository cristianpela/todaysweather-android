package home.crsk.todaysweather.android.dataroom.location;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * .
 * Created by Cristian Pela on 22/1/2018.
 */
@Entity(tableName = "location")
public final class LocationDTO {

    @PrimaryKey
    @ColumnInfo(name = "location_key")
    @NonNull
    private String locationKey = "";

    private String city;

    @ColumnInfo(name = "country_code")
    private String countryCode;

    private double latitude;

    private double longitude;

    private boolean favorite;

    @ColumnInfo(name = "record_update_time")
    public long recordUpdateTime;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @NonNull
    public String getLocationKey() {
        return locationKey;
    }

    public void setLocationKey(@NonNull String locationKey) {
        this.locationKey = locationKey;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationDTO that = (LocationDTO) o;

        return locationKey.equals(that.locationKey);
    }

    @Override
    public int hashCode() {
        return locationKey.hashCode();
    }

    @Override
    public String toString() {
        return "LocationDTO{" +
                "locationKey='" + locationKey + '\'' +
                ", city='" + city + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", favorite=" + favorite +
                ", recordUpdateTime=" + recordUpdateTime +
                '}';
    }
}

