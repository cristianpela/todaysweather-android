package home.crsk.todaysweather.android.dataroom.favorite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Single;

/**
 * .
 * Created by Cristian Pela on 22.02.2018.
 */
@Dao
public interface FavoriteDAO {

    String GET_ALL_QUERY = "SELECT " +
            "loc.location_key as location_key, " +
            "loc.city as city, " +
            "loc.favorite as favorite," +
            "f.last_updated as last_updated," +
            "hf.temperature_degree_metric as temperature_degree_metric," +
            "hf.temperature_degree_english as temperature_degree_english," +
            "hf.conditions as conditions," +
            "hf.icon as icon," +
            "hf.precipitation_chance as precipitation_chance," +
            "hf.year as year," +
            "hf.month as month," +
            "hf.day as day," +
            "hf.hour as hour," +
            "hf.time as time " +
            "FROM location loc " +
            "LEFT JOIN hour_forecast hf, forecast f ON " +
            "		  f.fk_location_key = loc.location_key AND " +
            "          hf.fk_forecast_id = f.id " +
            "		  AND (" +
            "		  strftime('%Y-%m-%d %H',hf.time / 1000, 'unixepoch','localtime') = strftime('%Y-%m-%d %H', 'now', 'localtime')" +
            "          OR " +
            "		  strftime('%Y-%m-%d %H',hf.time / 1000, 'unixepoch','localtime') = strftime('%Y-%m-%d %H', 'now', '+1 hour', 'localtime'))" +
            "WHERE loc.favorite = 1 " +
            "UNION " +
            "SELECT " +
            "loc.location_key as location_key, " +
            "loc.city as city, " +
            "loc.favorite as favorite," +
            "null, null, null, " +
            "null, null, null," +
            "null, null, null," +
            "null, null " +
            "FROM location loc WHERE loc.favorite = 1;";

    String GET_BY_KEY_QUERY = "SELECT " +
            "loc.location_key," +
            "loc.city," +
            "loc.favorite," +
            "vf.last_updated,"+
            "vf.temperature_degree_metric," +
            "vf.temperature_degree_english," +
            "vf.conditions," +
            "vf.icon," +
            "vf.precipitation_chance," +
            "vf.year," +
            "vf.month," +
            "vf.day," +
            "vf.hour," +
            "vf.time " +
            "FROM location loc " +
            "LEFT JOIN " +
            "(SELECT * FROM hour_forecast hf, forecast f WHERE " +
            "          hf.fk_forecast_id = f.id " +
            "		  AND (" +
            "		  strftime('%Y-%m-%d %H',hf.time / 1000, 'unixepoch','localtime') = strftime('%Y-%m-%d %H', 'now', 'localtime')" +
            "          OR " +
            "		  strftime('%Y-%m-%d %H',hf.time / 1000, 'unixepoch','localtime') = strftime('%Y-%m-%d %H', 'now', '+1 hour', 'localtime'))) " +
            "AS vf ON vf.fk_location_key = loc.location_key " +
            "WHERE loc.location_key =:locationKey ORDER BY vf.time LIMIT 1;";

    @Query(GET_ALL_QUERY)
    Single<List<FavoriteDTO>> getAll();

    @Query(GET_BY_KEY_QUERY)
    Single<FavoriteDTO> getByKey(String locationKey);

    @Query("UPDATE location SET favorite=:favorite, record_update_time=:recordUpdateTime WHERE location_key=:locationKey")
    void setFavorite(String locationKey, boolean favorite, long recordUpdateTime);

}
