package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
@SuppressWarnings("WeakerAccess")
@Entity(tableName = "hour_forecast",
        foreignKeys = @ForeignKey(
                entity = ForecastDTO.class,
                parentColumns = "id",
                childColumns = "fk_forecast_id",
                onDelete = ForeignKey.CASCADE
        ))
public final class HourForecastDTO extends BaseForecastDTO {

    @ColumnInfo(name = "temperature_degree_metric")
    public final int temperatureDegreeMetric;

    @ColumnInfo(name = "temperature_degree_english")
    public final int temperatureDegreeEnglish;

    public final String conditions;

    @ColumnInfo(name = "wind_speed_metric")
    public final int windSpeedMetric;

    @ColumnInfo(name = "wind_speed_english")
    public final int windSpeedEnglish;

    @ColumnInfo(name = "wind_direction")
    public final String windDirection;

    @ColumnInfo(name = "wind_degrees")
    public final int windDegrees;

    @ColumnInfo(name = "feels_like_metric")
    public final int feelsLikeMetric;

    @ColumnInfo(name = "feels_like_english")
    public final int feelsLikeEnglish;

    @ColumnInfo(name = "precipitation_chance")
    public final int precipitationChance;

    public final int humidity;

    public HourForecastDTO(long fkForecastId, long time, int year, int month, int day, int hour,
                           int temperatureDegreeMetric,
                           int temperatureDegreeEnglish, String conditions,
                           String icon, int windSpeedMetric, int windSpeedEnglish, String windDirection, int windDegrees, int feelsLikeMetric, int feelsLikeEnglish, int precipitationChance, int humidity) {
        super(fkForecastId, time, year, month, day, hour, icon);
        this.temperatureDegreeMetric = temperatureDegreeMetric;
        this.temperatureDegreeEnglish = temperatureDegreeEnglish;
        this.conditions = conditions;
        this.windSpeedMetric = windSpeedMetric;
        this.windSpeedEnglish = windSpeedEnglish;
        this.windDirection = windDirection;
        this.windDegrees = windDegrees;
        this.feelsLikeMetric = feelsLikeMetric;
        this.feelsLikeEnglish = feelsLikeEnglish;
        this.precipitationChance = precipitationChance;
        this.humidity = humidity;
    }
}
