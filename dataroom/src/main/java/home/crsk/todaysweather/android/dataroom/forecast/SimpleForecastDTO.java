package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
@SuppressWarnings("WeakerAccess")
@Entity(tableName = "simple_forecast",
        foreignKeys = @ForeignKey(
                entity = ForecastDTO.class,
                parentColumns = "id",
                childColumns = "fk_forecast_id",
                onDelete = ForeignKey.CASCADE
        ))
public final class SimpleForecastDTO extends BaseForecastDTO {

    @ColumnInfo(name = "high_metric")
    public final int highMetric;

    @ColumnInfo(name = "high_english")
    public final int highEnglish;

    @ColumnInfo(name = "low_metric")
    public final int lowMetric;

    @ColumnInfo(name = "low_english")
    public final int lowEnglish;

    public final String conditions;

    @ColumnInfo(name = "precipitation_chance")
    public final int precipitationChance;

    @ColumnInfo(name = "wind_speed_metric")
    public final int windSpeedMetric;

    @ColumnInfo(name = "wind_speed_english")
    public final int windSpeedEnglish;

    @ColumnInfo(name = "wind_direction")
    public final String windDirection;

    @ColumnInfo(name = "wind_degrees")
    public final int windDegrees;

    @ColumnInfo(name = "avg_wind_speed_metric")
    public final int windSpeedMetricAvg;

    @ColumnInfo(name = "avg_wind_speed_english")
    public final int windSpeedEnglishAvg;

    @ColumnInfo(name = "avg_wind_direction")
    public final String windDirectionAvg;

    @ColumnInfo(name = "avg_wind_degrees")
    public final int windDegreesAvg;

    public final int humidity;

    @ColumnInfo(name = "rain_quantity_metric")
    public final float rainQuantityMetric;

    @ColumnInfo(name = "rain_quantity_english")
    public final float rainQuantityEnglish;

    @ColumnInfo(name = "snow_quantity_metric")
    public final float snowQuantityMetric;

    @ColumnInfo(name = "snow_quantity_english")
    public final float snowQuantityEnglish;

    public SimpleForecastDTO(long fkForecastId,
                             long time, int year, int month, int day, int hour,
                             int highMetric, int highEnglish,
                             int lowMetric, int lowEnglish, String conditions,
                             String icon, int precipitationChance, int windSpeedMetric,
                             int windSpeedEnglish, String windDirection, int windDegrees,
                             int windSpeedMetricAvg, int windSpeedEnglishAvg,
                             String windDirectionAvg, int windDegreesAvg, int humidity,
                             float rainQuantityMetric, float rainQuantityEnglish,
                             float snowQuantityMetric, float snowQuantityEnglish) {
        super(fkForecastId, time, year, month, day, hour, icon);
        this.highMetric = highMetric;
        this.highEnglish = highEnglish;
        this.lowMetric = lowMetric;
        this.lowEnglish = lowEnglish;
        this.conditions = conditions;
        this.precipitationChance = precipitationChance;
        this.windSpeedMetric = windSpeedMetric;
        this.windSpeedEnglish = windSpeedEnglish;
        this.windDirection = windDirection;
        this.windDegrees = windDegrees;
        this.windSpeedMetricAvg = windSpeedMetricAvg;
        this.windSpeedEnglishAvg = windSpeedEnglishAvg;
        this.windDirectionAvg = windDirectionAvg;
        this.windDegreesAvg = windDegreesAvg;
        this.humidity = humidity;
        this.rainQuantityMetric = rainQuantityMetric;
        this.rainQuantityEnglish = rainQuantityEnglish;
        this.snowQuantityMetric = snowQuantityMetric;
        this.snowQuantityEnglish = snowQuantityEnglish;
    }
}
