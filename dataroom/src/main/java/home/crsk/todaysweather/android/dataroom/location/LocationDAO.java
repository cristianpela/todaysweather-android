package home.crsk.todaysweather.android.dataroom.location;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * .
 * Created by Cristian Pela on 22/1/2018.
 */
@Dao
public interface LocationDAO {

    @Query("SELECT * FROM location WHERE location_key=:key")
    Maybe<LocationDTO> findByKey(String key);

    @Query("SELECT * FROM location WHERE city LIKE :partialSearch")
    Maybe<List<LocationDTO>> find(String partialSearch);

    @Query("SELECT * FROM location WHERE city=:city AND country_code=:countryCode")
    Maybe<List<LocationDTO>> findExact(String city, String countryCode);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void save(List<LocationDTO> locations);

    @Update
    void update(LocationDTO location);

    @Query("SELECT * FROM location ORDER BY record_update_time DESC LIMIT 1")
    Flowable<LocationDTO> observeFavorites();

    @Query("SELECT * FROM location WHERE favorite=1")
    Single<List<LocationDTO>> findAllFavorites();

    @Query("SELECT * FROM location ORDER BY record_update_time DESC LIMIT 1")
    LocationDTO fetchLatestUpdated();
}
