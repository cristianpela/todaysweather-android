package home.crsk.todaysweather.android.dataroom;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * .
 * Created by Cristian Pela on 12.02.2018.
 */

public final class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? new Date(System.currentTimeMillis()) : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? System.currentTimeMillis() : date.getTime();
    }

}
