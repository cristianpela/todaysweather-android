package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
@SuppressWarnings("WeakerAccess")
public final class FullForecastDTO {

    @Embedded
    public ForecastDTO forecastMeta;

    @Relation(parentColumn = "id",
            entityColumn = "fk_forecast_id",
            entity = SimpleForecastDTO.class)
    public  List<SimpleForecastDTO> simpleForecasts;


    @Relation(parentColumn = "id",
            entityColumn = "fk_forecast_id",
            entity = TextForecastDTO.class)
    public List<TextForecastDTO> textForecasts;


    @Relation(parentColumn = "id",
            entityColumn = "fk_forecast_id",
            entity = HourForecastDTO.class)
    public  List<HourForecastDTO> hourForecasts;


}
