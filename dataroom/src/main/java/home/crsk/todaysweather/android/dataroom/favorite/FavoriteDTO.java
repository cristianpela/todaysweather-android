package home.crsk.todaysweather.android.dataroom.favorite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;
import android.support.annotation.Nullable;

/**
 * data class Favorite(val locationKey: String,
 * val city: String,
 * val isFavorite: Boolean,
 * val lastUpdate: Long,
 * val hour: TimeData? = null,
 * val hourTemperature: UnitPayload<Int>? = null,
 * val hourCondition: String? = null,
 * val hourConditionIcon: String? = null,
 * val hourPrecipitationChance: Int? = null,
 * val minTemperature: UnitPayload<Int>? = null,
 * val maxTemperature: UnitPayload<Int>? = null)
 * .
 * Created by Cristian Pela on 22.02.2018.
 */

public class FavoriteDTO {

    @ColumnInfo(name = "location_key")
    public String locationKey;

    public String city;

    public boolean favorite;

    @ColumnInfo(name = "last_updated")
    public long lastUpdate;

    @Nullable
    public Long time;

    @Nullable
    public Integer year;

    @Nullable
    public Integer month;

    @Nullable
    public Integer day;

    @Nullable
    public Integer hour;

    @ColumnInfo(name = "temperature_degree_metric")
    @Nullable
    public Integer temperatureDegreeMetric;

    @ColumnInfo(name = "temperature_degree_english")
    @Nullable
    public Integer temperatureDegreeEnglish;

    @ColumnInfo(name = "precipitation_chance")
    @Nullable
    public Integer precipitationChance;

    @Nullable
    public String conditions;

    @Nullable
    public String icon;

    @Ignore
    @Nullable
    public Integer highMetric;

    @Ignore
    @Nullable
    public Integer highEnglish;

    @Ignore
    @Nullable
    public Integer lowMetric;

    @Ignore
    @Nullable
    public Integer lowEnglish;

}
