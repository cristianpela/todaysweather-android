package home.crsk.todaysweather.android.dataroom;

import android.arch.persistence.room.RoomDatabase;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
public final class TransactionManager<DB extends RoomDatabase> {

    private DB db;

    public TransactionManager(DB db) {
        this.db = db;
    }

    public void execute(Runnable dbAction) throws Exception {
        db.runInTransaction(dbAction);
    }
}
