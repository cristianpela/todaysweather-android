package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

import home.crsk.todaysweather.android.dataroom.BaseDTO;
import home.crsk.todaysweather.android.dataroom.location.LocationDTO;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
@SuppressWarnings("WeakerAccess")
@Entity(tableName = "forecast",
        foreignKeys = @ForeignKey(
                entity = LocationDTO.class,
                parentColumns = "location_key",
                childColumns = "fk_location_key",
                onDelete = ForeignKey.CASCADE
        ))
public final class ForecastDTO extends BaseDTO {

    @ColumnInfo(name = "fk_location_key")
    public final String fkLocationKey;

    @ColumnInfo(name = "last_updated")
    public final long lastUpdated;

    public ForecastDTO(String fkLocationKey, long lastUpdated) {
        this.fkLocationKey = fkLocationKey;
        this.lastUpdated = lastUpdated;
    }
}
