package home.crsk.todaysweather.android.dataroom;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import home.crsk.todaysweather.android.dataroom.favorite.FavoriteDAO;
import home.crsk.todaysweather.android.dataroom.forecast.ForecastDAO;
import home.crsk.todaysweather.android.dataroom.forecast.ForecastDTO;
import home.crsk.todaysweather.android.dataroom.forecast.HourForecastDTO;
import home.crsk.todaysweather.android.dataroom.forecast.SimpleForecastDTO;
import home.crsk.todaysweather.android.dataroom.forecast.TextForecastDTO;
import home.crsk.todaysweather.android.dataroom.location.LocationDAO;
import home.crsk.todaysweather.android.dataroom.location.LocationDTO;
import home.crsk.todaysweather.android.dataroom.search.SearchHistoryDAO;
import home.crsk.todaysweather.android.dataroom.search.SearchHistoryDTO;

/**
 * .
 * Created by Cristian Pela on 22/1/2018.
 */
@Database(entities = {
        LocationDTO.class,
        SearchHistoryDTO.class,
        ForecastDTO.class,
        SimpleForecastDTO.class,
        TextForecastDTO.class,
        HourForecastDTO.class},
        version = 5)
@TypeConverters({Converters.class})
public abstract class TodaysWeatherDatabase extends RoomDatabase {

    public final static String DB_NAME = "todays_weather_db";

    public static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            String[] tables = {"location", "search_history", "forecast", "hour_forecast",
                    "simple_forecast", "text_forecast"};
            for (String table : tables) {
                database.execSQL("ALTER TABLE " + table +
                        " ADD COLUMN record_update_time INTEGER NOT NULL DEFAULT 0");
            }
        }
    };


    public static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE text_forecast ADD COLUMN forecast_text_english TEXT");

        }
    };

    public static TodaysWeatherDatabase create(Context context, String name, boolean inMemory) {
        Builder<TodaysWeatherDatabase> builder = (inMemory) ?
                Room.inMemoryDatabaseBuilder(context, TodaysWeatherDatabase.class)
                : Room.databaseBuilder(context, TodaysWeatherDatabase.class, name);
        return builder.addMigrations(MIGRATION_3_4, MIGRATION_4_5).build();
    }

    public abstract LocationDAO locationDao();

    public abstract FavoriteDAO favoriteDao();

    public abstract SearchHistoryDAO seaarchHistoryDao();

    public abstract ForecastDAO forecastDao();

}
