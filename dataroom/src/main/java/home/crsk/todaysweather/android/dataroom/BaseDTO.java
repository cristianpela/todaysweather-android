package home.crsk.todaysweather.android.dataroom;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.PrimaryKey;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */

public abstract class BaseDTO {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "record_update_time")
    public long recordUpdateTime;

}
