package home.crsk.todaysweather.android.dataroom.forecast;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

/**
 * .
 * Created by Cristian Pela on 31.01.2018.
 */
@SuppressWarnings("WeakerAccess")
@Entity(tableName = "text_forecast",
        foreignKeys = @ForeignKey(
                entity = ForecastDTO.class,
                parentColumns = "id",
                childColumns = "fk_forecast_id",
                onDelete = ForeignKey.CASCADE
        ))
public final class TextForecastDTO extends BaseForecastDTO {

    public final int period;

    public final String title;

    @ColumnInfo(name = "forecast_text")
    public final String textMetric;

    @ColumnInfo(name = "forecast_text_english")
    public final String textEnglish;

    @ColumnInfo(name = "precipitation_chance")
    public final int precipitationChance;

    public TextForecastDTO(long fkForecastId, long time, int year, int month, int day, int hour,
                           String icon, int period, String title, String textMetric, String textEnglish,
                           int precipitationChance) {
        super(fkForecastId, time, year, month, day, hour, icon);
        this.period = period;
        this.title = title;
        this.textMetric = textMetric;
        this.textEnglish = textEnglish;
        this.precipitationChance = precipitationChance;
    }
}
