package home.crsk.todaysweather.android.dataroom;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.arch.persistence.room.testing.MigrationTestHelper;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * .
 * Created by Cristian Pela on 12.02.2018.
 */

@RunWith(AndroidJUnit4.class)
public class MigrationTest {
    private static final String TEST_DB = "migration-test";

    @Rule
    public MigrationTestHelper helper;

    public MigrationTest() {
        helper = new MigrationTestHelper(InstrumentationRegistry.getInstrumentation(),
                TodaysWeatherDatabase.class.getCanonicalName(),
                new FrameworkSQLiteOpenHelperFactory());
    }

    @Test
    public void migrate1To4() throws IOException {
        SupportSQLiteDatabase db = helper.createDatabase(TEST_DB, 3);

        // db has schema version 3. insert some data using SQL queries.
        // You cannot use DAO classes because they expect the latest schema.

        String statement = "INSERT OR IGNORE INTO " +
                "`location`(`location_key`,`city`,`country_code`,`latitude`,`longitude`,`favorite`) VALUES (?,?,?,?,?,?)";
        SupportSQLiteStatement supportSQLiteStatement = db.compileStatement(statement);
        supportSQLiteStatement.bindString(1, "tm_key");
        supportSQLiteStatement.bindString(2, "Timisoara");
        supportSQLiteStatement.bindString(3, "RO");
        supportSQLiteStatement.bindDouble(4, 12345.022);
        supportSQLiteStatement.bindDouble(5, 12345.023);
        supportSQLiteStatement.bindLong(6, 1);
        supportSQLiteStatement.executeInsert();


        // Prepare for the next version.
        db.close();

        // Re-open the database with version 4 and provide
        // MIGRATION_3_4 as the migration process.
        db = helper.runMigrationsAndValidate(TEST_DB, 4, true,
                TodaysWeatherDatabase.MIGRATION_3_4);

        // MigrationTestHelper automatically verifies the schema changes,
        // but you need to validate that the data was migrated properly.
        Cursor c = db.query("SELECT * FROM location WHERE favorite=1 ORDER BY record_update_time LIMIT 1");
        if (c != null ) {
            if  (c.moveToFirst()) {
                do {
                    String key = c.getString(c.getColumnIndex("location_key"));
                    boolean isFavorite = c.getInt(c.getColumnIndex("favorite")) == 1;
                    long  recordTime = c.getInt(c.getColumnIndex("record_update_time"));
                    assertEquals("tm_key", key);
                    assertEquals(true, isFavorite);
                    assertEquals(0, recordTime);
                }while (c.moveToNext());
            }
            c.close();
        }

        db.close();
    }
}
