package home.crsk.todaysweather.android

import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.Koin
import org.koin.log.PrintLogger
import org.koin.standalone.KoinComponent
import org.koin.standalone.setProperty
import org.koin.test.KoinTest
import org.koin.test.dryRun

/**
 * .
 * Created by Cristian Pela on 24/12/2017.
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class AppKoinDepTest: KoinTest, KoinComponent{

    private val ctx = InstrumentationRegistry.getTargetContext()!!

    @Before
    fun before() {
        Koin.logger = PrintLogger()
    }

    @Test
    fun testDep() {
        setProperty(TodaysWeatherApplication.DI.ACTIVITY_CONTEXT, ctx)
        dryRun()
    }

}