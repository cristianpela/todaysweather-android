package home.crsk.todaysweather.android.services.autocomplete

import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import home.crsk.todaysweather.android.services.BaseDBTest
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import org.junit.Test

/**
 * .
 * Created by Cristian Pela on 5/1/2018.
 */
class AutoCompleteRepositoryLocalTest : BaseDBTest() {

    private lateinit var repository: AutoCompleteRepository

    override fun onInit(db: TodaysWeatherDatabase, txManager: TransactionManager<TodaysWeatherDatabase>) {
        repository = AutoCompleteRepositoryLocal(db.locationDao())
    }


    @Test
    fun findByKey() {
        initialData()
                .andThen(Maybe.defer {
                    repository.findByKey("b")
                })
                .test()
                .assertNoErrors()
                .assertValue(Location.bare("b"))
    }

    @Test
    fun should_just_complete_with_no_values_when_key_not_found() {
        initialData()
                .andThen(Maybe.defer {
                    repository.findByKey("d")
                })
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertNoValues()
    }


    @Test
    fun find() {
        initialData(listOf(
                Location("a", "ro", "key1"),
                Location("aaa", "ro", "key2"),
                Location("aaaa", "ro", "key3"),
                Location("b", "ro", "key4"),
                Location("baa", "ro", "key5")
        )).andThen(Maybe.defer {
            repository.find("a")
        }).test()
                .assertValue(listOf(
                        Location("a", "ro", "key1"),
                        Location("aaa", "ro", "key2"),
                        Location("aaaa", "ro", "key3")
                ))
    }

    @Test
    fun findExact() {
        initialData(listOf(Location("city", "ro", "key1")))
                .andThen(Maybe.defer {
                    repository.findExact("city", "ro")
                })
                .test()
                .assertValue(listOf(Location("city", "ro", "key1")))
    }

    private fun initialData(list: List<Location> = listOf("a", "b", "c").map { Location.bare(it) }): Completable {
        return repository
                .save(list)
                .toCompletable()
    }
}