@file:Suppress("IllegalIdentifier")

package home.crsk.todaysweather.android.services.forecast

import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import home.crsk.todaysweather.android.dataroom.forecast.ForecastDAO
import home.crsk.todaysweather.android.dataroom.location.LocationDAO
import home.crsk.todaysweather.android.services.BaseDBTest
import home.crsk.todaysweather.android.services.room.toDatabaseEntity
import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test

/**
 * .
 * Created by Cristian Pela on 01.02.2018.
 */
class ForecastRepositoryImplTest : BaseDBTest() {

    private lateinit var repository: ForecastRepository

    private lateinit var locationDAO: LocationDAO
    private lateinit var forecastDAO: ForecastDAO

    override fun onInit(db: TodaysWeatherDatabase, txManager: TransactionManager<TodaysWeatherDatabase>) {
        repository = ForecastRepositoryImpl(db.forecastDao(), txManager)
        locationDAO = db.locationDao()
    }

    // @Test
    fun getBatch() {
        fail()
    }

    //NOTE: https://www.reddit.com/r/androiddev/comments/6xkg4w/write_human_readable_android_tests_with_kotlin/dmgwu9t/
    @Test
    fun should_get_0_when_querying_last_update_for_a_non_existent_location() {
        repository.getLastUpdate("badkey").test().assertValue(0L)
    }

    @Test
    fun should_be_maybe_empty_when_batch_not_found() {
        repository.getBatch("badkey", UnitEnum.METRIC)
                .test().assertNoValues()
    }

    @Test
    fun saveBatch() {
        val location = Location("Timisoara", "RO", "1")
        val forecast = Forecast(
                listOf(
                        SimpleForecast(
                                TimeData(1, 1, 1, 1, 1),
                                UnitPayload.metric(1) { it.degreeSymbol },
                                UnitPayload.metric(1) { it.degreeSymbol },
                                "conditions1",
                                "icon1",
                                50,
                                WindSpeed(
                                        UnitPayload.metric(1) { it.speedMeasure },
                                        "N", 25),
                                WindSpeed(
                                        UnitPayload.metric(1) { it.speedMeasure },
                                        "N", 25),
                                10,
                                UnitPayload.metric(1.0f) { it.rainMeasure },
                                UnitPayload.metric(1.0f) { it.snowMeasure }
                        )
                ),
                listOf(
                        TextForecast(
                                TimeData(1, 1, 1, 1, 1),
                                1,
                                "icon",
                                "title",
                                UnitPayload.metric("textMetric"),
                                50
                        )
                ),
                listOf(
                        HourForecast(
                                TimeData(1, 1, 1, 1, 1),
                                UnitPayload.metric(1) { it.degreeSymbol },
                                "condition",
                                "icon",
                                WindSpeed(
                                        UnitPayload.metric(1) { it.speedMeasure },
                                        "N", 25),
                                UnitPayload.metric(1) { it.degreeSymbol },
                                10, 100
                        )

                ),
                0L
        )
        locationDAO.save(listOf(location.toDatabaseEntity()))

        val first = repository.saveBatch(location.locationKey, forecast).flatMapMaybe {
            repository.saveBatch(location.locationKey, forecast).flatMapMaybe {
                repository.getBatch(location.locationKey, UnitEnum.METRIC)
            }
        }.test().values().first()
        assertEquals(forecast, first)
    }

}