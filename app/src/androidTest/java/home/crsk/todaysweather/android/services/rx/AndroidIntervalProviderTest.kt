package home.crsk.todaysweather.android.services.rx

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 27.02.2018.
 */
@RunWith(AndroidJUnit4::class)
class AndroidIntervalProviderTest{

    val ctx = InstrumentationRegistry.getTargetContext()!!

    @Test
    fun interval() {
        val test = AndroidIntervalProvider(ctx)
                .interval(30, TimeUnit.SECONDS)
                .startWith(-1)
                .take(3)
                .test()
        test.awaitTerminalEvent()
        assertEquals(listOf<Long>(-1, 0, 1),test.values())
    }

}