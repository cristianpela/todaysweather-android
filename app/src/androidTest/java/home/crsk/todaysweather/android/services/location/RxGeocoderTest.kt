package home.crsk.todaysweather.android.services.location

import android.support.test.InstrumentationRegistry
import android.support.test.filters.MediumTest
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import java.lang.Thread.sleep

/**
 * .
 * Created by Cristian Pela on 4/1/2018.
 */
@RunWith(AndroidJUnit4::class)
@MediumTest
class RxGeocoderTest {

    private val ctx = InstrumentationRegistry.getTargetContext()!!

    @Test
    fun getAddressFromLocation() {
        sleep(3000)

        val test = RxGeocoder(ctx, AndroidSchedulers.mainThread())
                .getAddressFromLocation(45.673793, 21.166806)
                .test()


        val values = test.values()
        Log.d("RxGeocoderTest", values.toString())
        Log.d("RxGeocoderTest", test.errors().toString())
    }

}