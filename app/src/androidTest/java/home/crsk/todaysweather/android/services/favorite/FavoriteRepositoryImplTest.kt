package home.crsk.todaysweather.android.services.favorite

import android.arch.persistence.room.RoomDatabase
import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import home.crsk.todaysweather.android.dataroom.location.LocationDAO
import home.crsk.todaysweather.android.services.BaseDBTest
import home.crsk.todaysweather.android.services.room.toDatabaseEntity
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import io.reactivex.schedulers.Schedulers
import org.junit.Test

/**
 * .
 * Created by Cristian Pela on 13.02.2018.
 */
class FavoriteRepositoryImplTest : BaseDBTest() {

    private lateinit var repository: FavoriteRepository

    private lateinit var locationDao: LocationDAO

    private lateinit var db: RoomDatabase


    override fun onInit(db: TodaysWeatherDatabase, txManager: TransactionManager<TodaysWeatherDatabase>) {
        this.db = db
        locationDao = db.locationDao()
        repository = FavoriteRepositoryImpl(locationDao, db.favoriteDao())
        (repository as FavoriteRepositoryImpl).scheduler = Schedulers.trampoline()

        locationDao.save(listOf(
                Location.bare("foo1").copy(favorite = true).toDatabaseEntity().apply { recordUpdateTime =1 },
                Location.bare("foo2").copy(favorite = true).toDatabaseEntity().apply { recordUpdateTime =2 },
                Location.bare("foo3").copy(favorite = true).toDatabaseEntity().apply { recordUpdateTime =3 },
                Location.bare("bar1").toDatabaseEntity().apply { recordUpdateTime =4 },
                Location.bare("bar2").toDatabaseEntity().apply { recordUpdateTime =5 },
                Location.bare("bar3").toDatabaseEntity().apply { recordUpdateTime =6 }
        ))

    }

    @Test
    fun getAll() {

    }

    @Test
    fun setFavorite() {
        repository.setFavorite(Location.bare("bar1").copy(favorite = true)).andThen(
            repository.findByKey("bar1")
        ).test().assertValue(Location.bare("bar1").copy(favorite = true))
    }

    @Test
    fun observeChanges() {

        val test = repository.observeChanges().take(2).test()

        repository.setFavorite(Location.bare("bar1").copy(favorite = true)).subscribe()

        Thread.sleep(2000)

        Loggy.d(locationDao.fetchLatestUpdated().toString())

        repository.setFavorite(Location.bare("bar2").copy(favorite = true)).subscribe()

        Thread.sleep(3000)

       // test.assertValueAt(0,  Location.bare("bar1").copy(favorite = true))
       // test.assertValueAt(1,  Location.bare("bar2").copy(favorite = true))

        Loggy.d(locationDao.fetchLatestUpdated().toString())
    }

}