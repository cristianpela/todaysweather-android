package home.crsk.todaysweather.android.services.location

import android.content.Context
import android.location.Criteria
import android.location.Geocoder
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Build
import android.os.Bundle
import android.support.test.InstrumentationRegistry
import android.support.test.filters.MediumTest
import android.support.test.rule.GrantPermissionRule
import android.support.test.runner.AndroidJUnit4
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

/**
 * .
 * Created by Cristian Pela on 11/12/2017.
 */
@RunWith(AndroidJUnit4::class)
@MediumTest
class LocationDetectorImplTest {

    @Rule
    @JvmField
    val accessFineLocationRule: GrantPermissionRule = GrantPermissionRule
            .grant(android.Manifest.permission.ACCESS_FINE_LOCATION)

    @Rule
    @JvmField
    val accessCoarseLocationRule: GrantPermissionRule = GrantPermissionRule
            .grant(android.Manifest.permission.ACCESS_COARSE_LOCATION)


    private val ctx = InstrumentationRegistry.getTargetContext()!!

    private val manager by lazy { ctx.getSystemService(Context.LOCATION_SERVICE) as LocationManager }

    private val providers = listOf(
            object {
                val name = "CoarseProvider"
                val status = LocationProvider.AVAILABLE
                val enabled = true
                val powerRequirement = Criteria.POWER_LOW
                val accuracyRequirement = Criteria.ACCURACY_COARSE
            }
    )

    private fun clearTestProviders() =
            providers.map { provider ->
                with(manager) {
                    getProvider(provider.name)?.run {
                        clearTestProviderEnabled(provider.name)
                        clearTestProviderLocation(provider.name)
                        clearTestProviderStatus(provider.name)
                        removeTestProvider(provider.name)
                    }
                }
            }


    private fun pushLocation(provider: String, lat: Double, longi: Double) =
            manager.setTestProviderLocation(provider, DeviceLocation(provider).apply {
                latitude = lat
                longitude = longi
                time = System.currentTimeMillis()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    elapsedRealtimeNanos = System.nanoTime()
                }
                accuracy = 3.0f
            })

    @Test
    fun detect() {

        clearTestProviders()

        providers.map { provider ->
            with(manager) {
                addTestProvider(provider.name, false, false, false, false, false,
                        true, true, provider.powerRequirement, provider.accuracyRequirement)
                setTestProviderEnabled(provider.name, provider.enabled)
                setTestProviderStatus(provider.name, provider.status, Bundle(), System.currentTimeMillis())
            }
        }

        val currentScheduler = AndroidSchedulers.mainThread()
        val backoffScheduler = TestScheduler()
        val test = LocationDetectorImpl(RxGeocoder(ctx, backoffScheduler), manager).detect(false)
                .subscribeOn(currentScheduler)
                .observeOn(currentScheduler).test()

        pushLocation(providers.first().name, 45.7489, 21.2087)

        test.awaitTerminalEvent()

        println("Location values: ${test.values()}")

        test.assertNoErrors()

        clearTestProviders()

    }

}