package home.crsk.todaysweather.android.services

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith

/**
 * .
 * Created by Cristian Pela on 01.02.2018.
 */
@RunWith(AndroidJUnit4::class)
abstract class BaseDBTest {

    private lateinit var db: TodaysWeatherDatabase

    private val txManager by lazy { TransactionManager<TodaysWeatherDatabase>(db)}

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        val ctx = InstrumentationRegistry.getTargetContext()!!
        db = TodaysWeatherDatabase.create(ctx, TodaysWeatherDatabase.DB_NAME, true)
        onInit(db, txManager)

    }

    @After
    fun tearDown() = db.close()

    abstract fun onInit(db: TodaysWeatherDatabase, txManager: TransactionManager<TodaysWeatherDatabase>)

}