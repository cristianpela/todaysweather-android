package home.crsk.todaysweather.android.services.location

import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import home.crsk.todaysweather.android.services.BaseDBTest
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import io.reactivex.Maybe
import org.junit.Test

/**
 * .
 * Created by Cristian Pela on 5/1/2018.
 */
class LocationRepositoryImplTest: BaseDBTest() {

    private lateinit var repository: LocationRepository

    override fun onInit(db: TodaysWeatherDatabase, txManager: TransactionManager<TodaysWeatherDatabase>) {
        repository = LocationRepositoryImpl(db.locationDao())
    }


    @Test
    fun findByKey() {
        repository
                .save(listOf("a", "b", "c").map { Location.bare(it) })
                .toCompletable()
                .andThen(Maybe.defer {
                    repository.findByKey("b")
                })
                .test()
                .assertNoErrors()
                .assertValue(Location.bare("b"))
    }

    @Test
    fun should_just_complete_with_no_values_when_key_not_found() {
        repository
                .save(listOf("a", "b", "c").map { Location.bare(it) })
                .toCompletable()
                .andThen(Maybe.defer {
                    repository.findByKey("d")
                })
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertNoValues()
    }

}