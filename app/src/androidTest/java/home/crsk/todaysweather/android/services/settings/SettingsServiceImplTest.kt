package home.crsk.todaysweather.android.services.settings

import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.test.InstrumentationRegistry
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.setup.usecases.NoSetupError
import io.reactivex.Completable
import org.junit.*

import org.junit.Assert.*
import org.junit.runner.RunWith
import java.lang.Thread.sleep

/**
 * .
 * Created by Cristian Pela on 19/12/2017.
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class SettingsServiceImplTest {

    lateinit var prefs: SharedPreferences
    val svc: SettingsService by lazy { SettingsServiceImpl(prefs) }

    @Before
    fun setUp() {
        prefs = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry
                .getTargetContext()!!)
    }

    @After
    fun tearDown() {
        prefs.edit().clear().apply()
    }

    @Test
    fun check() {
        svc.check().test().assertError(NoSetupError)
        Completable.defer{svc.save(Settings("foo", "bar", UnitEnum.ENGLISH))}
                .andThen(Completable.defer { svc.check()})
                .test().assertComplete()
    }

    @Test
    fun observe() {
        val test = svc.observe().take(3).test()

        Completable.defer{ svc.save(Settings("foo", "bar", UnitEnum.ENGLISH))}
                .andThen(Completable.defer{svc.save(Settings("foo2", "bar2", UnitEnum.METRIC))})
                .subscribe()

        test.awaitTerminalEvent()

        test.assertValueAt(0, Settings())
                .assertValueAt(1, Settings("foo", "bar", UnitEnum.ENGLISH))
                .assertValueAt(2, Settings("foo2", "bar2", UnitEnum.METRIC))

    }


}