package home.crsk.todaysweather.android

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import home.crsk.todaysweather.android.presentation.HasPresentation
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.State
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 19/12/2017.
 */
abstract class BaseActivity<VM : ViewModel, in S : State>
    : LocalizationContextAwareActivity(), IntentRenderer<S> {

    companion object {
        const val ANDROID_INTENT_KEY_ARGUMENTS = "ANDROID_INTENT_KEY_ARGUMENTS"
    }

    private lateinit var viewModel: VM

    private val hasPresentationHandler = HasPresentationHandler<S>()

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(id())

//        if (BuildConfig.DEBUG) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (!Settings.canDrawOverlays(applicationContext)) {
//                    val aIntent = AndroidIntent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
//                            Uri.parse("package:" + getPackageName()))
//                    startActivityForResult(aIntent, 112)
//                } else {
//                    LogcatViewer.showLogcatLoggerView(this)
//                }
//            }else{
//                LogcatViewer.showLogcatLoggerView(this)
//            }
//
//        }


        val firstTime = savedInstanceState == null
        hasPresentationHandler.updateBundles(savedInstanceState,
                if (firstTime) intent.getBundleExtra(ANDROID_INTENT_KEY_ARGUMENTS) else null)
        viewModel = ViewModelProviders.of(this).get(viewModelBlueprint())
        if (viewModel is HasPresentation<*>) {
            hasPresentationHandler.allowRendererToPrepareViews(this)
        } else {
            throw IllegalStateException("Your ViewModel must implement HasPresentation")
        }

    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: android.content.Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (BuildConfig.DEBUG) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (requestCode == 112) {
//                    if (Settings.canDrawOverlays(this)) {
//                        LogcatViewer.showLogcatLoggerView(this);
//                    }
//                }
//            }
//        }
//    }

    @CallSuper
    override fun onStart() {
        hasPresentationHandler.subscribeStreams(viewModel as HasPresentation<*>, this)
        super.onStart()

    }

    @CallSuper
    override fun onStop() {
        hasPresentationHandler.disposeStreams(this)
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putAll(hasPresentationHandler.getLaterSavingInstance())
        super.onSaveInstanceState(outState)
    }

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) = Unit

    override fun onArgumentsPassed(args: Bundle) = Unit

    fun addToLaterSavingInstance(key: String, value: Any) = hasPresentationHandler
            .addToLaterSavingInstance(key, value)

    fun removeFromLaterFromSavingInstance(key: String) = hasPresentationHandler
            .removeFromLaterFromSavingInstance(key)

    fun sendIntentMaybePending(intent: Intent) = hasPresentationHandler.sendIntentMaybePending(intent)


    fun post(action: () -> Unit) = hasPresentationHandler.post(action)

    fun postDelayed(value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS, action: () -> Unit): Long =
            hasPresentationHandler.postDelayed(value, unit, action)


    fun cancelAndDelay(token: Long? = null, value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS, action: () -> Unit): Long {
        clearPostAction(token)
        return hasPresentationHandler.postDelayed(value, unit, action)
    }

    fun clearPostAction(token: Long?) {
        if (token != null)
            hasPresentationHandler.clearPostAction(token)
    }

    fun clearAllPostActions() {
        hasPresentationHandler.clearAllPostActions()
    }

    @LayoutRes
    abstract fun id(): Int

    abstract fun viewModelBlueprint(): Class<VM>

}

