package home.crsk.todaysweather.android.services.location

import home.crsk.todaysweather.android.dataroom.location.LocationDAO
import home.crsk.todaysweather.android.services.room.toDatabaseEntity
import home.crsk.todaysweather.android.services.room.toDomainEntity
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * .
 * Created by Cristian Pela on 20/12/2017.
 */
class LocationRepositoryImpl(private val dao: LocationDAO) : LocationRepository {

    override fun findByKey(key: String): Maybe<Location> = Maybe.defer {
        dao.findByKey(key)
    }.subscribeOn(Schedulers.io()).map { it.toDomainEntity() }

    override fun save(locations: List<Location>): Single<List<Location>> =
            Completable.create {
                dao.save(locations.map { it.toDatabaseEntity() })
                it.onComplete()
            }.subscribeOn(Schedulers.io()).andThen(Single.just(locations))
}

