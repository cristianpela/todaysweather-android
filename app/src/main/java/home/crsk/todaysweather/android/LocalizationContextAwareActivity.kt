package home.crsk.todaysweather.android

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.franmontiel.localechanger.LocaleChanger
import org.koin.Koin
import org.koin.android.ext.android.releaseContext
import org.koin.android.ext.android.releaseProperties
import org.koin.android.ext.android.setProperty
import org.koin.standalone.KoinComponent
import org.koin.standalone.getProperty
import java.util.*


/**
 * Adapter for LocalizationActivity and ContextAwareActivity
 * Created by Cristian Pela on 19/12/2017.
 */
abstract class LocalizationContextAwareActivity()
    : AppCompatActivity(), KoinComponent {

    abstract val contextName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        setProperty(TodaysWeatherApplication.DI.ACTIVITY_CONTEXT, this.baseContext) // injecting the activity context
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        Koin.logger.log("Activity ContextAware - drop on onDestroy")
        releaseProperties(TodaysWeatherApplication.DI.ACTIVITY_CONTEXT)
        if (contextName != Scopes.SCOPE_LESS)
            releaseContext(contextName)
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleChanger.configureBaseContext(newBase))
    }

}
