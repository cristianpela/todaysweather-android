package home.crsk.todaysweather.android.presentation.setup

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import com.jakewharton.rxbinding2.view.RxView
import com.squareup.picasso.Picasso
import home.crsk.todaysweather.android.*
import home.crsk.todaysweather.android.Scopes.CTX_SETUP_AUTOMATIC_LOCATION
import home.crsk.todaysweather.domain.common.entity.simpleGoogleStaticMapUrl
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screens
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.setup.SetupState
import io.reactivex.Observable

import kotlinx.android.synthetic.main.fragment_setup_auto_location.*
import kotlinx.android.synthetic.main.included_setup_location_map.*
import kotlinx.android.synthetic.main.included_setup_title_progress.*
import kotlinx.android.synthetic.main.item_location.*
import org.koin.android.ext.android.inject

/**
 * .
 * Created by Cristian Pela on 27/12/2017.
 */

class AutomaticSetupFragment : BaseFragment<SetupAutoViewModel, SetupState>() {

    override val contextName: String = CTX_SETUP_AUTOMATIC_LOCATION

    private val KEY_LOCATION_KEY = "KEY_LOCATION_ID"

    private val picasso by inject<Picasso>()

    override fun onAllowViewsToPrepare() {
        textSetupTitle.setText(R.string.setup_step_title_location_auto)
        includeItemLocation.setBackgroundColor(Color.argb(200, 255, 255, 255))
        if (activity.isInLandscape()) {
            includeSetupTitle.visibility = View.GONE
            includeItemLocation.visibility = View.GONE
            groupButtons.visibility = View.GONE
        }
    }

    override fun renderState(state: SetupState) {
        if (!activity.isInLandscape()) {
            progressSetup.visibility = if (state.inFlight) View.VISIBLE else View.INVISIBLE
            btnSetupNext.visibility = if (state.inFlight) View.INVISIBLE else View.VISIBLE
            includeItemLocation.visibility = if (state.inFlight || (!state.inFlight && state.selectedLocation == null))
                View.GONE else View.VISIBLE

            btnLocationSave.isEnabled = state.completed
            btnLocationManual.isEnabled = state.completed
            btnLocationSearch.isEnabled = state.completed
        }
        state.selectedLocation?.apply {
            if (!activity.isInLandscape()) {
                txtLocationName.text = city
                txtLocationLat.text = String.format("%.4f", latitude)
                txtLocationLong.text = String.format("%.4f", longitude)
            }
            val mapUrl = simpleGoogleStaticMapUrl(null)
            picasso.customLoad(mapUrl, imageLocationBigMap){
                placeholder( R.drawable.map_placeholder).noFade()
            }
            addToLaterSavingInstance(KEY_LOCATION_KEY, locationKey)
            btnSetupNext.tag = state
        }
    }

    override fun renderMessages(message: CommonMessage) = Unit

    override fun renderErrors(err: Throwable) {
        includeItemLocation.visibility = GONE
        btnSetupNext.visibility = INVISIBLE
        val errMessage = if (err.message.isNullOrEmpty()) context.getString(R.string.err_query_not_found) else err.message
        Snackbar.make(containerSetupAuto, errMessage.toString(), Snackbar.LENGTH_SHORT)
                .show()
    }

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) {
        intentSender.sendIntent(Intent.InitIntent(savedInstanceState.getString(KEY_LOCATION_KEY)))
    }

    override fun provideIntentStreams(): List<Observable<Intent>> = listOf(
            RxView.clicks(btnLocationSearch).map { Intent.RetryIntent },
            RxView.clicks(btnLocationManual).map {
                Intent.NavigationIntent(
                        Route(Screens.SCREEN_AUTOMATIC_SETUP, Screens.SCREEN_MANUAL_SETUP))
            },
            RxView.clicks(btnSetupNext)
                    .map { btnSetupNext.tag as SetupState }
                    .switchMap {
                        rxAlert(context,
                                R.string.alert_dialog_general_title,
                                R.string.alert_dialog_general_yes_btn,
                                R.string.alert_dialog_general_no_btn,
                                R.string.alert_dialog_message_setup_location_next, it.selectedLocation!!.city)
                    }.filter { confirm -> confirm }.map {
                Intent.SaveIntent(btnSetupNext.tag)
            }).map { it.cast(Intent::class.java) }


    override fun id(): Int = R.layout.fragment_setup_auto_location

    override fun viewModelBlueprint(): Class<SetupAutoViewModel> = SetupAutoViewModel::class.java

    override fun onDestroy() {
        picasso.cancelRequest(imageLocationBigMap)
        super.onDestroy()
    }
}
