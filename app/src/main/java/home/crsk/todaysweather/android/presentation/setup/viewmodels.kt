package home.crsk.todaysweather.android.presentation.setup

import android.arch.lifecycle.ViewModel
import android.support.v7.util.DiffUtil
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.android.presentation.HasPresentation
import home.crsk.todaysweather.android.presentation.autocomplete.LocationDiffCallback
import home.crsk.todaysweather.domain.autocomplete.AutoCompleteSearchPresentation
import home.crsk.todaysweather.domain.autocomplete.AutoCompleteSearchState
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.presentation.NoState
import home.crsk.todaysweather.domain.common.presentation.PresentationStreamOps
import home.crsk.todaysweather.domain.common.presentation.decorate
import home.crsk.todaysweather.domain.main.MainPresentation
import home.crsk.todaysweather.domain.main.NavigationState
import home.crsk.todaysweather.domain.common.presentation.State
import home.crsk.todaysweather.domain.forecast.presentation.ForecastPresentation
import home.crsk.todaysweather.domain.forecast.presentation.ForecastState
import home.crsk.todaysweather.domain.setup.SetupState
import home.crsk.todaysweather.domain.setup.presentation.*
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 20/12/2017.
 */

abstract class BaseViewModel<S : State> : ViewModel(), KoinComponent,
        HasPresentation<PresentationStreamOps<S>>

class MainViewModel : BaseViewModel<NavigationState>() {
    override val presentation by inject<MainPresentation>()
}

class LanguageSetupViewModel : BaseViewModel<LanguageSetupState>() {
    override val presentation by inject<LanguageSetupPresentation>()
}

class SetupTypeChooseViewModel : BaseViewModel<NoState>() {
    override val presentation by inject<SetupTypeChoosePresentation>()
}

class SetupAutoViewModel : BaseViewModel<SetupState>() {
    override val presentation by inject<AutomaticSetupPresentation>()
}

class SetupManualViewModel : BaseViewModel<SetupState>() {
    override val presentation by inject<ManualSetupPresentation>()
}


class ForecastViewModel : BaseViewModel<TransientForecastState>() {
    private val original by inject<ForecastPresentation>()
    override val presentation = original.decorate(
            ObservableTransformer<State, TransientForecastState> {
                //skip initial state and cast
                it.skip(1).cast(ForecastState::class.java).scan(TransientForecastState()) { acc, next ->

                    val isValid = next.todayForecast != null

                    val todayForecast = next.todayForecast?.takeIf {
                        it != acc.accumulated?.todayForecast
                    }

                    val hourForecast = next.hourForecast?.takeIf {
                        it != acc.accumulated?.hourForecast
                    }

                    val threeDaysHourForecast = next.threeDaysHourForecasts?.takeIf {
                        it != acc.accumulated?.threeDaysHourForecasts
                    }

                    val hourIndex = next.hourForecastIndex?.takeIf {
                        it != acc.accumulated?.hourForecastIndex
                    }

                    val location = next.currentLocation?.takeIf {
                        it != acc.accumulated?.currentLocation
                    }

                    val isFresh = next.isFresh

                    val updated = next.lastUpdate?.takeIf {
                        it != acc.accumulated?.lastUpdate
                    }

                    val unit = next.unit?.takeIf {
                        it != acc.accumulated?.unit
                    }
                    TransientForecastState(next, ForecastState(
                            todayForecast,
                            hourForecast,
                            threeDaysHourForecast,
                            hourIndex,
                            location,
                            updated,
                            isFresh,
                            unit), isValid)
                            .apply { if (next.inFlight) inFlight() else completed() }
                }.skip(1)
            }
    )
}

//todo refactor
class AutoCompleteSearchViewModel : BaseViewModel<DiffedAutoCompleteSearchState>() {
    private val original by inject<AutoCompleteSearchPresentation>()
    override val presentation = original.decorate(
            ObservableTransformer<State, DiffedAutoCompleteSearchState> {
                it.cast(AutoCompleteSearchState::class.java)
                        //avoid state spam from upstream
                        //we got switching threads for free, because debounce defaults on Computation
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .scan(DiffedAutoCompleteSearchState()) { curr, next ->
                            val sortedNextLocation = next.locations.sortedBy { it.city }
                            val cb = LocationDiffCallback(curr.diffResultWrap.second, sortedNextLocation)
                            val result = DiffUtil.calculateDiff(cb, true)
                            DiffedAutoCompleteSearchState(next.partialSearch, result to sortedNextLocation, next.owner)
                                    .apply { if (next.inFlight) inFlight() else completed() }
                        }
                        .skip(1)
                        .observeOn(AndroidSchedulers.mainThread())
            }
    )
}

data class DiffedAutoCompleteSearchState(
        val partialSearch: String = "",
        val diffResultWrap: Pair<DiffUtil.DiffResult, List<Location>> = LocationDiffCallback.EMPTY_DIFF_RESULT to emptyList(),
        val owner: String? = null
) : State()


data class TransientForecastState(
        internal val accumulated: ForecastState? = null,
        val diffedForecastState: ForecastState? = null,
        val isValid: Boolean = false
) : State()