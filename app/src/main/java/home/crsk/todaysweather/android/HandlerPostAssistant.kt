package home.crsk.todaysweather.android

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.support.graphics.drawable.AnimationUtilsCompat
import android.support.v7.widget.ViewUtils
import android.view.animation.AnimationUtils
import home.crsk.todaysweather.domain.common.log.Loggy
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong


/**
 * Wraps a handler and provides better control over handler methods via self backed tokens
 * Created by user on 30.01.2018.
 */
class HandlerPostAssistant {

    private val tokenGenerator = AtomicLong()

    private val handler = Handler(Looper.getMainLooper())

    fun post(action: () -> Unit): Long = postDelayed(0, action = action)

   // fun postNextFrame(action: () -> Unit): Long = postDelayed(ViewUtils.ge)

    fun postDelayed(value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS, action: () -> Unit): Long {
        val token = tokenGenerator.getAndIncrement()
        val futureTime = SystemClock.uptimeMillis() + unit.toMillis(value)
        handler.postAtTime(action, token, futureTime)
        return token
    }

    fun clearAllActions() = handler.removeCallbacksAndMessages(null)

    fun clearAction(token: Long) = handler.removeCallbacksAndMessages(token)
}