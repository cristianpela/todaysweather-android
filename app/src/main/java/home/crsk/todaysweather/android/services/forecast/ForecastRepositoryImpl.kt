package home.crsk.todaysweather.android.services.forecast

import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import home.crsk.todaysweather.android.dataroom.forecast.*
import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.entity.UnitEnum.*
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * .
 * Created by Cristian Pela on 30.01.2018.
 */
class ForecastRepositoryImpl(
        private val dao: ForecastDAO,
        private val txManager: TransactionManager<TodaysWeatherDatabase>) : ForecastRepository {

    override fun clear(locationKey: String): Completable = Completable.complete()

    override fun getBatch(locationKey: String, unit: UnitEnum): Maybe<Forecast> =
            dao.getMostRecentFullForecast(locationKey).filter{
                it.hourForecasts.isNotEmpty() && it.simpleForecasts.isNotEmpty() && it.textForecasts.isNotEmpty()
            }.map {
                Loggy.d("ForecastRepository - Local: Load Forecast for location key $it in unit: $unit")
                Forecast(it.simpleForecasts.asSequence().map { it.toDomainDTO(unit) }.toList(),
                        it.textForecasts.asSequence().map { it.toDomainDTO(unit) }.toList(),
                        it.hourForecasts.asSequence().map { it.toDomainDTO(unit) }.toList(),
                        it.forecastMeta.lastUpdated)
            }

    override fun getLastUpdate(locationKey: String): Single<Long> =
            dao.getLastUpdateTime(locationKey).onErrorReturnItem(0L)

    override fun saveBatch(locationKey: String, forecast: Forecast): Single<Forecast> =
            Completable.create {
                try {
                    Loggy.d("ForecastRepository - Local: Saving Forecast for location key $locationKey time: ${forecast.lastUpdate}")
                    txManager.execute {
                        dao.deleteForecastsForLocation(locationKey)

                        val id = dao.insertForecast(forecast.toDatabaseDTO(locationKey))
                        dao.insertTextForecast(
                                forecast.textForecasts
                                        .asSequence()
                                        .map { it.toDatabaseDTO(id) }
                                        .toList())
                        dao.insertSimpleForecasts(
                                forecast.simpleForecasts
                                        .asSequence()
                                        .map { it.toDatabaseDTO(id) }
                                        .toList())
                        dao.insertHourForecasts(
                                forecast.hourlyForecasts
                                        .asSequence()
                                        .map { it.toDatabaseDTO(id) }
                                        .toList())
                        it.onComplete()
                    }
                } catch (ex: Exception) {
                    it.onError(ex)
                }
            }.andThen(Single.just(forecast))

}

private fun Forecast.toDatabaseDTO(locationKey: String) = ForecastDTO(locationKey, lastUpdate)

private fun TextForecast.toDatabaseDTO(forecastId: Long) = TextForecastDTO(
        forecastId,
        time.time,
        time.year,
        time.month,
        time.day,
        time.hour,
        icon,
        period,
        title,
        text.obtain().value,
        text.obtainOther().value,
        precipitationChance)

private fun TextForecastDTO.toDomainDTO(unit: UnitEnum) = TextForecast(
        TimeData(time, year, month, day, hour),
        period,
        icon,
        title,
        UnitPayload.decide(unit, valueMetric = textMetric, valueEnglish = textEnglish?:"?"),
        precipitationChance)

private fun SimpleForecast.toDatabaseDTO(forecastId: Long) = SimpleForecastDTO(
        forecastId,
        time.time,
        time.year,
        time.month,
        time.day,
        time.hour,
        high.obtain().value,
        high.obtainOther().value,
        low.obtain().value,
        low.obtainOther().value,
        conditions,
        icon,
        precipitationChance,
        maxWindSpeed.speed.obtain().value,
        maxWindSpeed.speed.obtainOther().value,
        maxWindSpeed.direction,
        maxWindSpeed.degrees,
        averageWindSpeed.speed.obtain().value,
        averageWindSpeed.speed.obtainOther().value,
        averageWindSpeed.direction,
        averageWindSpeed.degrees,
        humidity,
        rainQuantity.obtain().value,
        rainQuantity.obtainOther().value,
        snowQuantity.obtain().value,
        snowQuantity.obtainOther().value)

private fun SimpleForecastDTO.toDomainDTO(unit: UnitEnum) = SimpleForecast(
        TimeData(time, year, month, day, hour),
        UnitPayload.decide(unit, highMetric, highEnglish) { it.degreeSymbol },
        UnitPayload.decide(unit, lowMetric, lowEnglish) { it.degreeSymbol },
        conditions,
        icon,
        precipitationChance,
        WindSpeed(UnitPayload
                .decide(unit, windSpeedMetric, windSpeedEnglish) { it.speedMeasure },
                windDirection,
                windDegrees),
        WindSpeed(UnitPayload
                .decide(unit, windSpeedMetricAvg, windSpeedEnglishAvg) { it.speedMeasure },
                windDirectionAvg,
                windDegreesAvg),
        humidity,
        UnitPayload.decide(unit, rainQuantityMetric, rainQuantityEnglish) { it.rainMeasure },
        UnitPayload.decide(unit, snowQuantityMetric, snowQuantityEnglish) { it.snowMeasure })

private fun HourForecast.toDatabaseDTO(forecastId: Long) = HourForecastDTO(
        forecastId,
        time.time,
        time.year,
        time.month,
        time.day,
        time.hour,
        temperatureDegree.obtain().value,
        temperatureDegree.obtainOther().value,
        condition,
        icon,
        windSpeed.speed.obtain().value,
        windSpeed.speed.obtainOther().value,
        windSpeed.direction,
        windSpeed.degrees,
        feelsLike.obtain().value,
        feelsLike.obtainOther().value,
        precipitationChance,
        humidity)

private fun HourForecastDTO.toDomainDTO(unit: UnitEnum) = HourForecast(
        TimeData(time, year, month, day, hour),
        UnitPayload.decide(unit, temperatureDegreeMetric, temperatureDegreeEnglish) { it.degreeSymbol },
        conditions,
        icon,
        WindSpeed(UnitPayload
                .decide(unit, windSpeedMetric, windSpeedEnglish) { it.speedMeasure },
                windDirection,
                windDegrees),
        UnitPayload.decide(unit, feelsLikeMetric, feelsLikeEnglish) { it.degreeSymbol },
        precipitationChance, humidity)

fun <T> UnitPayload.Companion.decide(unit: UnitEnum,
                                             valueMetric: T,
                                             valueEnglish: T,
                                             symbolProvider: (unit: UnitEnum) -> String = { _ -> "" }): UnitPayload<T> {

    return if (unit == ENGLISH)
        UnitPayload.english(valueEnglish, symbolProvider)
    else
        UnitPayload.metric(valueMetric, symbolProvider)
}