package home.crsk.todaysweather.android.services.rx

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_CANCEL_CURRENT
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.SystemClock
import home.crsk.todaysweather.android.AndroidIntent
import io.reactivex.Observable
import java.lang.IllegalArgumentException
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

/**
 * .
 * Created by Cristian Pela on 27.02.2018.
 */

interface IntervalProvider {

    fun interval(period: Long, unit: TimeUnit, initialDelay: Long = 0L): Observable<Long>

}

class AndroidIntervalProvider(private val context: Context) : IntervalProvider {

    companion object {
        const val ACTION_INTERVAL_LOCAL = "home.crsk.todaysweather.action.INTERVAL_LOCAL"
        const val MINIMUM_INTERVAL_MILLIS = 30000
    }


    override fun interval(period: Long, unit: TimeUnit, initialDelay: Long): Observable<Long> =
            Observable.create<Long> {
                if(unit.toMillis(period) < 30000)
                    throw IllegalArgumentException("AndroidIntervalProvider - " +
                            "Interval for native alarm manager must be at least 30 seconds!")

                val count = AtomicLong()

                //register broadcast receiver
                val broadcastIntentFilter = IntentFilter(ACTION_INTERVAL_LOCAL)
                val receiver: BroadcastReceiver = object : BroadcastReceiver() {
                    override fun onReceive(context: Context?, intent: Intent?) {
                        it.onNext(count.getAndIncrement())
                    }
                }
                context.registerReceiver(receiver, broadcastIntentFilter)

                //register alarm
                val alarmManager: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val alarmIntent = AndroidIntent().apply { action = ACTION_INTERVAL_LOCAL }
                val alarmPendingIntent = PendingIntent.getBroadcast(context, 1337, alarmIntent,
                        FLAG_CANCEL_CURRENT)
                alarmManager.setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        System.currentTimeMillis() + unit.toMillis(initialDelay),
                        unit.toMillis(period),
                        alarmPendingIntent)

                it.setCancellable {
                    alarmManager.cancel(alarmPendingIntent)
                    alarmPendingIntent.cancel()
                    context.unregisterReceiver(receiver)
                }
            }


}