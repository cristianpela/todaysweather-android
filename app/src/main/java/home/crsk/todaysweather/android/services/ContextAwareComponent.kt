package home.crsk.todaysweather.android.services

import org.koin.standalone.KoinComponent

/**
 * .
 * Created by Cristian Pela on 17.02.2018.
 */
interface ContextAwareComponent: KoinComponent {

    val contextName: String

}