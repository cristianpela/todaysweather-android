package home.crsk.todaysweather.android.presentation.forecast

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.graphics.drawable.TransitionDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.text.util.LinkifyCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import home.crsk.todaysweather.android.*
import home.crsk.todaysweather.android.presentation.autocomplete.KEY_SEARCH_OWNER
import home.crsk.todaysweather.android.presentation.autocomplete.KEY_SEARCH_SELECT_LOCATION_KEY
import home.crsk.todaysweather.android.presentation.setup.ForecastViewModel
import home.crsk.todaysweather.android.presentation.setup.TransientForecastState
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screens
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.googleStaticMaps
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import io.reactivex.Observable
import kotlinx.android.synthetic.main.action_view_forecast_unit_toggle.*
import kotlinx.android.synthetic.main.fragment_forecast.*
import kotlinx.android.synthetic.main.fragment_forecast_detail.*
import org.koin.android.ext.android.inject
import java.util.concurrent.TimeUnit


/**
 * .
 * Created by Cristian Pela on 30.01.2018.
 */
class ForecastFragment : BaseFragment<ForecastViewModel, TransientForecastState>() {

    override val contextName: String = Scopes.CTX_FORECAST

    companion object {
        private const val TRANSITION_DRAWABLE_DURATION = 1000

    }

    private var smoothScrollPostToken: Long? = null

    private val picasso by inject<Picasso>()


    @SuppressLint("InflateParams")
    override fun onAllowViewsToPrepare() {
        toolbarForecast.inflateMenu(R.menu.menu_forecast)


        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(recyclerHourForecast)
        recyclerHourForecast.adapter = HourForecastAdapter()
        recyclerNextDaysSummary.adapter = NextDaysSummaryAdapter()
        toolbarForecast.setOnMenuItemClickListener {

            when (it.itemId) {
                R.id.action_search -> sendIntentMaybePending(Intent.NavigationIntent(Route(
                        Screens.SCREEN_APP,
                        Screens.SCREEN_SEARCH,
                        mapOf(KEY_SEARCH_OWNER to Screens.SCREEN_APP),
                        true
//                        Route.TransitionInfo.withoutSharedElement(
//                                R.anim.slide_out_down,
//                                R.anim.slide_in_up)
                )))

                R.id.action_favorites -> sendIntentMaybePending(Intent.NavigationIntent(Route(
                        Screens.SCREEN_APP,
                        Screens.SCREEN_FAVORITES,
                        addToBackStack = true
                )))

                R.id.action_about -> {
                    val aboutView = LayoutInflater.from(context).inflate(R.layout.dialog_about, null)
                    aboutView.findViewById<ImageButton>(R.id.imageButtonWU).setOnClickListener{
                        context.applicationContext.startActivity(AndroidIntent(AndroidIntent.ACTION_VIEW,
                                        Uri.parse(getString(R.string.wu_url))))
                    }
                    aboutView.findViewById<TextView>(R.id.textAbout).movementMethod = LinkMovementMethod.getInstance()
                    AlertDialog.Builder(context)
                            .setView(aboutView)
                            .setCancelable(true)
                            .create()
                            .show()
                }

                else -> Unit
            }
            if (it.itemId == R.id.action_search) {

            }
            false
        }
        fabLocationFavorite.setOnClickListener {
            //todo move to domain usecase the favorite toggle
            sendIntentMaybePending(Intent.FavoriteChangeIntent(it.tag as Location))
        }

        with(swipeRefreshLayoutForecast) {
            isEnabled = false
            setColorSchemeResources(R.color.colorAccent)
            setDistanceToTriggerSync(300)
            setOnRefreshListener {
                sendIntentMaybePending(Intent.RetryIntent) // fetch forced from remote server
                swipeRefreshLayoutForecast.isRefreshing = false
            }
        }

        contentLoadingProgressBarForecast.indeterminateDrawable.colorFilter(context)
        contentLoadingProgressBarForecast.initLoadingState()


        switchTextMetric.text = UnitEnum.METRIC.degreeSymbol
        switchTextEnglish.text = UnitEnum.ENGLISH.degreeSymbol


    }


    private fun changeColorFreshness(isFresh: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (collapsingToolbarLayoutForecast.tag == null) {
                val id = if (isFresh) R.drawable.collapsible_toolbar_scrim_overlay_transition else
                    R.drawable.collapsible_toolbar_scrim_overlay_transition_inverse
                imageCollapsingMap.foreground = context.getDrawable(id)
                collapsingToolbarLayoutForecast.tag = isFresh
            }
            //check last value of freshness differs the new state
            if (collapsingToolbarLayoutForecast.tag != isFresh) {
                (imageCollapsingMap.foreground as TransitionDrawable).apply {
                    startTransition(TRANSITION_DRAWABLE_DURATION)
                }
                collapsingToolbarLayoutForecast.tag = isFresh
            }
        }
        collapsingToolbarLayoutForecast.contentScrim?.setColorFilter(
                context.getColorCompat(
                        if (isFresh) R.color.colorFresh else R.color.colorOutdated
                ), PorterDuff.Mode.SRC_ATOP)

    }

    override fun renderState(state: TransientForecastState) {

        val diffState = state.diffedForecastState

        val context = this@ForecastFragment.context

        Loggy.d("ForecastFragment IN FLIGHT ${diffState?.inFlight}" )

        //cancel any previous ongoing state rendering like image load, animations, looper posts
        clearPostAction(smoothScrollPostToken)
        Picasso.with(context).cancelRequest(imageCollapsingMap)


        if (state.inFlight) {
            contentLoadingProgressBarForecast.pushLoadingState { show() }
            // swipeRefreshLayoutForecast.pushLoadingState { isEnabled = false }
        } else if (state.completed) {
            contentLoadingProgressBarForecast.popLoadingState { hide() }
            //swipeRefreshLayoutForecast.popLoadingState { isEnabled = true }
        }


        swipeRefreshLayoutForecast.isEnabled = true

        includeForecastDetail.toggleVisibility(state.isValid)

        forecastVisibilityGroup.visibility = View.VISIBLE

        diffState?.threeDaysHourForecasts?.let {
            (recyclerHourForecast.adapter as HourForecastAdapter).changeItems(it)
        }

        diffState?.hourForecastIndex?.let {
            if (it != RecyclerView.NO_POSITION)
            //todo maybe use recycler view layout post animation
                smoothScrollPostToken = post {
                    recyclerHourForecast.smoothScrollToPosition(it)
                    (recyclerHourForecast.adapter as HourForecastAdapter).setCurrentSelectedIndex(it)
                }

        }


        diffState?.currentLocation?.apply {
            collapsingToolbarLayoutForecast.title = city

            val mapUrl = googleStaticMaps(null, latitude, longitude) {
                markers {
                    this + marker(latitude, longitude)
                }
                map {
                    size {
                        width = screenSize.first
                        height = 600
                    }
                }

            }

            if (imageCollapsingMap.tag == null || imageCollapsingMap.tag as String != locationKey) {
                Loggy.d("ForecastFragment loading location image $this")
                picasso.cancelRequest(imageCollapsingMap)
                contentLoadingProgressBarForecast.pushLoadingState {
                    show()
                }
                val cb: Callback = object : Callback {
                    override fun onSuccess() {
                        imageCollapsingMap.tag = locationKey
                        contentLoadingProgressBarForecast.popLoadingState {
                            hide()
                        }
                    }

                    override fun onError() {
                        imageCollapsingMap.tag = null
                        contentLoadingProgressBarForecast.popLoadingState {
                            hide()
                        }
                    }
                }
                picasso.customLoad(mapUrl, imageCollapsingMap, cb)
            }

            fabLocationFavorite.setImageDrawable(context.getDrawable(
                    if (favorite)
                        R.drawable.ic_star_filled_lime_a100_24dp
                    else
                        R.drawable.ic_star_white_24dp))

            fabLocationFavorite.tag = this
        }

        diffState?.lastUpdate?.let {
            textLastUpdate.text = getString(R.string.forecast_update_label, it)
            textLastUpdate.setTextColor(context.getColorCompat(
                    if (diffState.isFresh) android.R.color.darker_gray
                    else R.color.colorOutdated))
            changeColorFreshness(diffState.isFresh)
        }

        diffState?.hourForecast?.let {
            textHour.text = it.hourDisplay.toUpperCase()
            textTemp.text = it.hourTempDisplay
            textConditions.text = it.hourCondition
            imageConditions.setImageDrawable(context.getWeatherConditionDrawable(it.hourIconUrl))

        }

        diffState?.todayForecast?.let {
            textMinMaxTemp.text = it.tempMinMaxDisplay
            textDayDetailsDayDescription.text = it.textDay
            textDayDetailsNightDescription.text = it.textNight
            textTodayCondition.text = it.todayCondition
            textTodayPrepChance.text = it.todayPrecipitationChance
            textTodayHumidity.text = it.todayHumidity
            textTodayWind.text = it.todayWindSpeed

            imageTodayRainQ.toggleVisibility(it.todayRainQuantity != null)
            it.todayRainQuantity?.let { textTodayRainQ.text = it }

            imageTodaySnowQ.toggleVisibility(it.todaySnowQuantity != null)
            it.todaySnowQuantity?.let { textTodaySnowQ.text = it }

            imageTodayCondition.setImageDrawable(context.getWeatherConditionDrawable(it.todayIconUrl))
            imageDayDetailDayIcon.setImageDrawable(context.getWeatherConditionDrawable(it.textDayIcon))
            imageDayDetailNightIcon.setImageDrawable(context.getWeatherConditionDrawable(it.textNightIcon))

            textNextDaysSummaryTitle.toggleVisibility(it.nextDaysSummary.isNotEmpty(), View.INVISIBLE)
            (recyclerNextDaysSummary.adapter as NextDaysSummaryAdapter).changeItems(it.nextDaysSummary)
        }


        diffState?.unit?.let {
            switchMetricEnglish.isChecked = it == UnitEnum.ENGLISH
            switchMetricEnglish.tag = it
        }

    }

    override fun onResume() {
        super.onResume()
        /**
        since on domain we have an interval of 1 hour to check forecast,
        when device goes to PAUSE + SLEEP the interval thread will stop
        (interval work on active code),

        https://github.com/ReactiveX/RxAndroid/issues/257#issuecomment-164263215

        so we need to retrigger the interval from a PAUSE state and after device WAKEs UP
        on domain a debounce operator guards for spamming ForecastWatchIntent. The spam happens
        when we subscribe onViewCreated lifecycle and here onResume (2 request in a row)

        another way might be to adapt code to use an alarm manager/job scheduler
        but that's unreliable because of inexact Repeat
         */
        sendIntentMaybePending(Intent.ForecastWatchIntent)
    }

    override fun onDestroyView() {
        picasso.cancelRequest(imageCollapsingMap)
        super.onDestroyView()
    }

    override fun renderMessages(message: CommonMessage) = Unit

    override fun provideIntentStreams(): List<Observable<Intent>> =
            listOf(RxCompoundButton.checkedChanges(switchMetricEnglish)
                    .skipInitialValue()
                    .filter { switchMetricEnglish.tag != null }
                    .map { Intent.UnitChangeIntent((switchMetricEnglish.tag as UnitEnum).other()) }
                    .distinctUntilChanged { key -> key.unit }
                    .cast(Intent::class.java),
                    RxView.clicks(textTemp) // manual hour refresh from local storage
                            .debounce(300, TimeUnit.MILLISECONDS)
                            .map { Intent.ForecastWatchIntent }
                            .cast(Intent::class.java)
            )

    override fun id(): Int = R.layout.fragment_forecast

    override fun viewModelBlueprint(): Class<ForecastViewModel> = ForecastViewModel::class.java

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) {
        intentSender.sendIntent(Intent.ForecastWatchIntent)
        intentSender.sendIntent(Intent.FavoriteWatchIntent)
    }

    override fun onArgumentsPassed(args: Bundle) {
        val locationKey = args.getString(KEY_SEARCH_SELECT_LOCATION_KEY)
        if (locationKey != null) {
            sendIntentMaybePending(Intent.SelectLocationIntent(locationKey))
        }
    }

    override fun renderErrors(err: Throwable) {
        swipeRefreshLayoutForecast.popLoadingState { isEnabled = false }
        contentLoadingProgressBarForecast.popLoadingState { hide() }
        super.renderErrors(err)
    }
}