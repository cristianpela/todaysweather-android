package home.crsk.todaysweather.android.services.favorite

import home.crsk.todaysweather.android.dataroom.favorite.FavoriteDAO
import home.crsk.todaysweather.android.dataroom.favorite.FavoriteDTO
import home.crsk.todaysweather.android.dataroom.location.LocationDAO
import home.crsk.todaysweather.android.services.forecast.decide
import home.crsk.todaysweather.android.services.room.toDomainEntity
import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.now
import home.crsk.todaysweather.domain.common.other.timeData
import home.crsk.todaysweather.domain.common.other.timeDiff
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.lang.UnsupportedOperationException
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 30.01.2018.
 */
class FavoriteRepositoryImpl(private val locationDAO: LocationDAO,
                             private val favoriteDAO: FavoriteDAO,
                             private val timeProvider: () -> TimeData = { now().timeData() }) : FavoriteRepository {

    var scheduler = Schedulers.io()

    override fun getFavorite(locationKey: String, unit: UnitEnum): Single<Favorite> = Single.defer {
        favoriteDAO.getByKey(locationKey)
    }.subscribeOn(scheduler).map { it.toDomainEntity(unit) }

    override fun findByKey(key: String): Maybe<Location> = Maybe.defer {
        locationDAO.findByKey(key)
    }.subscribeOn(scheduler).map { it.toDomainEntity() }

    override fun getAll(unit: UnitEnum): Single<List<Favorite>> = Single.defer {
        favoriteDAO.getAll()
    }.subscribeOn(scheduler)
            .map {
                it.asSequence()
                        .groupBy { it.locationKey }
                        .map {
                            it.value.asSequence().reduce { acc, favoriteDTO ->
                                //priority: hour NOW -> hour NOW + 1 -> NULL
                                val now = timeProvider()
                                val favTime: Long? = favoriteDTO.time
                                if ((acc.time == null && favTime == null) ||
                                        (favTime != null && favTime.timeData().isInTodayHour(now)) ||
                                        (acc.time == null && favTime!=null &&
                                                favTime.timeDiff(now.time, TimeUnit.SECONDS) >= 0)
                                ) {
                                    favoriteDTO
                                } else  {
                                    acc
                                }
                            }
                        }
                        .map { it.toDomainEntity(unit) }
                        .toList()
            }

    override fun setFavorite(location: Location): Completable = Completable.create {
        favoriteDAO.setFavorite(location.locationKey, location.favorite, timeProvider().time)
        Loggy.d("FavoriteRepository Location Favorite for ${location.locationKey}-${location.city} was changed to ${location.favorite}")
        it.onComplete()
    }.subscribeOn(scheduler)

    override fun save(locations: List<Location>): Single<List<Location>> = throw UnsupportedOperationException()

    override fun observeChanges(): Observable<Location> =
            Observable.defer {
                locationDAO.observeFavorites().toObservable()
            }.subscribeOn(scheduler).doOnSubscribe {
                Loggy.d("FavoriteRepository Observing Favorite Changes")
            }.doOnNext {
                        Loggy.d("FavoriteRepository Observed Location Favorite for ${it.locationKey}-${it.city}")
                    }.map { it.toDomainEntity() }
}

fun TimeData.isInTodayHour(other: TimeData): Boolean =
        this.year == other.year && this.month == other.month && this.day == other.day
                && this.hour == other.hour



fun FavoriteDTO.toDomainEntity(unitEnum: UnitEnum): Favorite {
    val locationKey = this.locationKey
    val city = this.city
    val favorite = this.favorite
    val lastUpdate = this.lastUpdate
    val hourTimeData = if(time == null) null else TimeData.fromPieces(year!!, month!!, day!!, hour!!)
    val hourTemp = temperatureDegreeMetric?.let {
        UnitPayload.decide<Int>(unitEnum, it, temperatureDegreeEnglish ?: Int.MAX_VALUE) {
            it.degreeSymbol
        }
    }
    val hourPrepChance = precipitationChance
    val hourConditions = conditions
    val hourIcon = icon
    val lowTemp = lowMetric?.let {
        UnitPayload.decide(unitEnum, it, lowEnglish ?: Int.MAX_VALUE) {
            it.degreeSymbol
        }
    }
    val highTemp = highMetric?.let {
        UnitPayload.decide(unitEnum, it, highEnglish ?: Int.MAX_VALUE) {
            it.degreeSymbol
        }
    }
    return Favorite(locationKey,
            city,
            favorite,
            lastUpdate,
            hourTimeData,
            hourTemp,
            hourConditions,
            hourIcon,
            hourPrepChance,
            lowTemp,
            highTemp)
}
