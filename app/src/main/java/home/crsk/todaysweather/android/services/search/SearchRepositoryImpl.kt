package home.crsk.todaysweather.android.services.search

import home.crsk.todaysweather.android.dataroom.search.SearchHistoryDAO
import home.crsk.todaysweather.android.services.room.toDatabaseEntity
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.SearchRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 *
 * Created by Cristian Pela on 11/12/2017.
 */
class SearchRepositoryImpl(private val dao: SearchHistoryDAO) : SearchRepository {

    override fun find(partialSearch: String): Maybe<String> = Maybe.defer {
        dao.find(partialSearch)
    }

    override fun save(partialSearch: String): Single<String> =
            Completable.create {
                dao.save(partialSearch.toDatabaseEntity())
                it.onComplete()
            }.andThen(Single.just(partialSearch))
}