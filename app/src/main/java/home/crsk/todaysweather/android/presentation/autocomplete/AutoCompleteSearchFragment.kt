package home.crsk.todaysweather.android.presentation.autocomplete

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.MenuItem
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.jakewharton.rxbinding2.support.v7.widget.RxToolbar
import home.crsk.todaysweather.android.*
import home.crsk.todaysweather.android.presentation.setup.AutoCompleteSearchViewModel
import home.crsk.todaysweather.android.presentation.setup.DiffedAutoCompleteSearchState
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_auto_complete_search.*
import java.util.concurrent.TimeUnit


class AutoCompleteSearchFragment : BaseDialogFragment<AutoCompleteSearchViewModel, DiffedAutoCompleteSearchState>() {

    override val contextName: String = Scopes.SCOPE_LESS

    private val recyclerAdapter by lazy { AutoCompleteSearchAdapter() }

    private var showProgressDelayToken: Long? = null

    private val searchMenuItem = fun(): MenuItem =
            toolbarAutocomplete.menu.findItem(R.id.action_search)

    init {
        setFullScreen = true
    }


    private val searchView = fun(): android.support.v7.widget.SearchView =
            searchMenuItem().actionView as android.support.v7.widget.SearchView


    override fun onAllowViewsToPrepare() {
        toolbarAutocomplete.inflateMenu(R.menu.menu_search_autocomplete)
        toolbarAutocomplete.setNavigationIcon(R.drawable.ic_close_white_24dp)
        recyclerAutocomplete.setupVerticalWithAdapter(recyclerAdapter)
        progressAutocomplete.indeterminateDrawable.colorFilter(context)
        recyclerAutocomplete.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    override fun onArgumentsPassed(args: Bundle) {
        args.getString(KEY_SEARCH_PARTIAL)?.let {
            searchView().setQuery(it, false)
        }
        searchMenuItem().expandActionView()
        searchView().tag = args.getString(KEY_SEARCH_OWNER)
    }

    override fun renderState(state: DiffedAutoCompleteSearchState) {
        showProgressDelayToken = delayedToggleVisibility(showProgressDelayToken, state, progressAutocomplete)
        recyclerAdapter.setLocations(state.owner, state.diffResultWrap)
    }

    override fun renderMessages(message: CommonMessage) {
        val selectMessage = message as CommonMessage.AutoCompleteSelectLocationMessage
        submitAndExit(selectMessage.locationKey, selectMessage.owner).map {
            sendIntentMaybePending(it)
        }
    }

    override fun provideIntentStreams(): List<Observable<Intent>> = listOf(
            RxSearchView
                    .queryTextChangeEvents(searchView())
                    .distinctUntilChanged()
                    .filter { it.queryText().length >= 3 }
                    .debounce(500, TimeUnit.MILLISECONDS)
                    .map { Intent.SearchLocationIntent(it.queryText().toString(), searchView().tag?.toString()) },
            RxToolbar.navigationClicks(toolbarAutocomplete)
                    .flatMap { Observable.fromArray(*submitAndExit()) },
            recyclerAdapter.observeSelection()
                    .map { Intent.SelectLocationIntent(it.second.locationKey, it.first) }
    ).map { it.cast(Intent::class.java) }

    override fun id(): Int = R.layout.fragment_auto_complete_search

    override fun viewModelBlueprint(): Class<AutoCompleteSearchViewModel> = AutoCompleteSearchViewModel::class.java

    private fun submitAndExit(locationKey: String? = null, owner: String? = null): Array<out Intent> {
        val backIntent: Intent = if (locationKey == null && owner == null) {
            Intent.NavigationIntent(Route.back(null))
        } else {
            Intent.NavigationIntent(
                    Route.back(mapOf(
                            KEY_SEARCH_OWNER to owner!! as Any,
                            KEY_SEARCH_SELECT_LOCATION_KEY to locationKey!!
                    ))
            )
        }
        return arrayOf(Intent.CancelIntent, Intent.ResetIntent, backIntent)
    }
}
