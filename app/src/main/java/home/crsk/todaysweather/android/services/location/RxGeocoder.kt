package home.crsk.todaysweather.android.services.location

import android.content.Context
import android.location.Address
import android.location.Geocoder
import home.crsk.todaysweather.android.TranslatableIOError
import home.crsk.todaysweather.android.stripDiacritics
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.LocationNotDetectedError
import home.crsk.todaysweather.domain.common.other.errorBackOffObservable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.*

/**
 * .
 * Created by Cristian Pela on 4/1/2018.
 */
class RxGeocoder(context: Context, private val scheduler: Scheduler = Schedulers.computation()) {

    private val geocoder = Geocoder(context, Locale.ENGLISH)

    fun getAddressFromLocation(latitude: Double, longitude: Double, maxResults: Int = 5): Observable<Address> =
            Observable.create<Address> { emitter ->
                try {
                    val addresses = geocoder.getFromLocation(latitude, longitude, maxResults)
                            .filter {
                                it.locality != null || (it.featureName != null && it.thoroughfare == null
                                            && it.featureName != it.countryName)
                            }
                            .map {
                                //make sure we have locality
                                it.locality = if (it.locality == null) it.featureName.stripDiacritics()
                                    else it.locality.stripDiacritics()
                                it //psh... mutable address
                            }
                            .distinctBy { it.locality }

                    if (addresses.isEmpty()) {
                        emitter.onError(
                                LocationNotDetectedError(Error(), LanguageProvider.ERR_LOCATION_NO_PLACE_DECODED))
                    } else {
                        addresses.forEach {
                            emitter.onNext(it)
                        }
                        emitter.onComplete()
                    }
                } catch (e: IllegalArgumentException) {
                    emitter.onError(e)
                } catch (e: IOException) {
                    emitter.onError(TranslatableIOError(e, LanguageProvider.ERR_CONNECTION))
                }
            }.retryWhen(errorBackOffObservable(skip = arrayListOf(
                    LocationNotDetectedError::class.java,
                    IllegalArgumentException::class.java
            ), delayScheduler = scheduler))

}


