package home.crsk.todaysweather.android

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.location.LocationManager
import android.support.multidex.MultiDexApplication
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.franmontiel.localechanger.LocaleChanger
import com.squareup.leakcanary.AndroidExcludedRefs
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import com.squareup.picasso.Picasso
import home.crsk.todaysweather.android.dataroom.TodaysWeatherDatabase
import home.crsk.todaysweather.android.dataroom.TransactionManager
import home.crsk.todaysweather.android.services.autocomplete.AutoCompleteRepositoryLocal
import home.crsk.todaysweather.android.services.favorite.FavoriteRepositoryImpl
import home.crsk.todaysweather.android.services.forecast.ForecastRepositoryImpl
import home.crsk.todaysweather.android.services.location.LocationDetectorImpl
import home.crsk.todaysweather.android.services.location.LocationRepositoryImpl
import home.crsk.todaysweather.android.services.location.RxGeocoder
import home.crsk.todaysweather.android.services.resources.ResourceFinderImpl
import home.crsk.todaysweather.android.services.rx.SchedulersContractImpl
import home.crsk.todaysweather.android.services.search.SearchRepositoryImpl
import home.crsk.todaysweather.android.services.settings.SettingsServiceImpl
import home.crsk.todaysweather.domain.autocomplete.AutoCompleteSearchPresentation
import home.crsk.todaysweather.domain.common.gateway.*
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepositoryProxy
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.SearchRepository
import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRemoteRepository
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepository
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacade
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacadeImpl
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsServiceSavingProxy
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.log.Seed
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.common.usecases.autocomplete.AutoCompleteUseCase
import home.crsk.todaysweather.domain.common.usecases.autocomplete.SelectedAutoCompleteUseCase
import home.crsk.todaysweather.domain.common.usecases.navigation.NavigationToUseCase
import home.crsk.todaysweather.domain.common.usecases.navigation.NavigationWatchUseCase
import home.crsk.todaysweather.domain.favorites.FavoriteState
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManagerImpl
import home.crsk.todaysweather.domain.favorites.presentation.FavoritesPresentation
import home.crsk.todaysweather.domain.favorites.usecases.FavoriteChangeUseCase
import home.crsk.todaysweather.domain.favorites.usecases.FavoriteWatchUseCase
import home.crsk.todaysweather.domain.favorites.usecases.FavoritesFetchUseCase
import home.crsk.todaysweather.domain.forecast.presentation.ForecastPresentation
import home.crsk.todaysweather.domain.forecast.usecases.ForecastUpdateUseCase
import home.crsk.todaysweather.domain.forecast.usecases.ForecastWatchUseCase
import home.crsk.todaysweather.domain.main.MainPresentation
import home.crsk.todaysweather.domain.settings.usecases.SettingsSaveUseCase
import home.crsk.todaysweather.domain.setup.presentation.AutomaticSetupPresentation
import home.crsk.todaysweather.domain.setup.presentation.LanguageSetupPresentation
import home.crsk.todaysweather.domain.setup.presentation.ManualSetupPresentation
import home.crsk.todaysweather.domain.setup.presentation.SetupTypeChoosePresentation
import home.crsk.todaysweather.domain.setup.usecases.AutomaticSetupUseCase
import home.crsk.todaysweather.domain.setup.usecases.LanguageSetupUseCase
import home.crsk.todaysweather.domain.setup.usecases.SetupCheckUseCase
import home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete.AutoCompleteRepositoryRetrofit
import home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete.autoCompleteService
import home.crsk.todaysweather.drivers.data.remote.retrofit.forecast.ForecastRemoteRepositoryRetrofit
import home.crsk.todaysweather.drivers.data.remote.retrofit.forecast.forecastService
import io.fabric.sdk.android.Fabric
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import timber.log.Timber
import java.util.*


/**
 *
 * Created by Cristian Pela on 7/12/2017.
 */
class TodaysWeatherApplication : MultiDexApplication() {


    private var refWatcher: RefWatcher? = null

    companion object {
        fun getRefWatcher(context: Context) = (context.applicationContext as TodaysWeatherApplication).refWatcher
    }


    class DI {

        companion object {
            const val ACTIVITY_CONTEXT = "ACTIVITY_CONTEXT"
            const val WU_API_KEY = "WU_API_KEY"

            const val REPO_AUTOCOMPLETE_REMOTE = "REPO_AUTOCOMPLETE_REMOTE"
            const val REPO_AUTOCOMPLETE_LOCAL = "REPO_AUTOCOMPLETE_LOCAL"

            const val REPO_AUTOCOMPLETE_PROXY = "REPO_AUTOCOMPLETE_PROXY"
            const val USE_CASE_AUTOCOMPLETE = "USE_CASE_AUTOCOMPLETE"

            const val USE_CASE_SELECTED_AUTOCOMPLETE = "USE_CASE_SELECTED_AUTOCOMPLETE"

            const val USE_CASE_CHECK = "USE_CASE_CHECK"
            const val USE_CASE_LANGUAGE_SETUP = "USE_CASE_LANGUAGE_SETUP"
            const val USE_CASE_SETTINGS_SAVE = "USE_CASE_SETTINGS_SAVE"


            const val USE_CASE_NAVIGATION_WATCH = "USE_CASE_NAVIGATION_WATCH"
            const val USE_CASE_NAVIGATION_TO = "USE_CASE_NAVIGATION_TO"

            const val USE_CASE_AUTOMATIC_SETUP = "USE_CASE_AUTOMATIC_SETUP"


            const val SVC_SETTINGS_DEFAULT = "SVC_SETTINGS_DEFAULT"
            const val SVC_SETTINGS_PROXY = "SVC_SETTINGS_PROXY"


            const val USE_CASE_FORECAST_UPDATE = "USE_CASE_FORECAST_UPDATE"
            const val USE_CASE_FORECAST_WATCH = "USE_CASE_FORECAST_WATCH"
            const val USE_CASE_FAVORITE_WATCH = "USE_CASE_FAVORITE_WATCH"
            const val USE_CASE_FAVORITE_CHANGE = "USE_CASE_FAVORITE_CHANGE"
            const val USE_CASE_FAVORITE_FETCH = "USE_CASE_FAVORITE_FETCH"


        }

        @Suppress("unused")
        private val neverUseCase: UseCase = object : UseCase {
            override fun execute(): ObservableTransformer<in Request, out Response> {
                return ObservableTransformer<Request, Response> { upstream -> upstream.switchMap { Observable.never<Response>() } }
            }

        }

        private val autoCompleteModule: Module = applicationContext {
            provide { SearchRepositoryImpl(get<TodaysWeatherDatabase>().seaarchHistoryDao()) as SearchRepository }
            provide { autoCompleteService }
            provide(REPO_AUTOCOMPLETE_REMOTE) { AutoCompleteRepositoryRetrofit(get()) as AutoCompleteRepository }
            provide(REPO_AUTOCOMPLETE_LOCAL) { AutoCompleteRepositoryLocal(get<TodaysWeatherDatabase>().locationDao()) as AutoCompleteRepository }
            provide(REPO_AUTOCOMPLETE_PROXY) {
                AutoCompleteRepositoryProxy(
                        get<AutoCompleteRepositoryRetrofit>(REPO_AUTOCOMPLETE_LOCAL),
                        get<AutoCompleteRepositoryLocal>(REPO_AUTOCOMPLETE_REMOTE),
                        get(),
                        get())
                        as AutoCompleteRepository
            }

            provide(USE_CASE_AUTOCOMPLETE) {
                AutoCompleteUseCase(get<AutoCompleteRepositoryProxy>(REPO_AUTOCOMPLETE_PROXY)) as UseCase
            }
            provide(USE_CASE_SELECTED_AUTOCOMPLETE) {
                SelectedAutoCompleteUseCase(get<AutoCompleteRepositoryProxy>(REPO_AUTOCOMPLETE_PROXY)) as UseCase
            }
        }

        private val rxModule: Module = applicationContext {
            provide { SchedulersContractImpl() as SchedulersContract }
        }

        private val roomModule: Module = applicationContext {
            provide {
                TodaysWeatherDatabase.create(androidApplication().applicationContext,
                        TodaysWeatherDatabase.DB_NAME, false)
            }
            provide { TransactionManager<TodaysWeatherDatabase>(get()) }
        }

        private val androidServicesModule: Module = applicationContext {
            provide {
                androidApplication().applicationContext
                        .getSharedPreferences("app_settings", Context.MODE_PRIVATE)
            }
        }

        private val locationModule = applicationContext {
            provide { RxGeocoder(androidApplication().applicationContext) }
            provide {
                androidApplication().applicationContext
                        .getSystemService(Context.LOCATION_SERVICE) as LocationManager
            }
            provide { LocationDetectorImpl(get(), get()) } bind LocationDetector::class
            provide { LocationRepositoryImpl(get<TodaysWeatherDatabase>().locationDao()) as LocationRepository }
        }

        private val forecastModule = applicationContext {
            provide { forecastService }
            provide { ForecastRepositoryImpl(get<TodaysWeatherDatabase>().forecastDao(), get()) as ForecastRepository }
            //todo watch out for lastUpdate in constructor - this is singleton, so no new upates/ or leave it as factory
            factory { ForecastRemoteRepositoryRetrofit(getProperty(WU_API_KEY), get()) as ForecastRemoteRepository }
            provide { ForecastRepositoryFacadeImpl(get(), get(), get()) as ForecastRepositoryFacade }
        }

        private val favoriteModule = applicationContext {
            provide {
                FavoriteRepositoryImpl(
                        get<TodaysWeatherDatabase>().locationDao(),
                        get<TodaysWeatherDatabase>().favoriteDao()
                ) as FavoriteRepository
            }
            factory {
                UndoFavoriteManagerImpl(get()) as UndoFavoriteManager
            }
        }

        private val presentationModule: Module = applicationContext {

            provide(SVC_SETTINGS_DEFAULT) { SettingsServiceImpl(get()) as SettingsService }
            provide(SVC_SETTINGS_PROXY) {
                SettingsServiceSavingProxy(get(SVC_SETTINGS_DEFAULT)) as SettingsService
            }

            provide {
                Picasso.Builder(androidApplication().applicationContext)
                        .listener { _, _, exception ->
                            Toast.makeText(androidApplication().applicationContext,
                                    exception.message.toString(),
                                    Toast.LENGTH_SHORT).show()
                        }
                        .enableDebugFeats()
                        .build()
            }

            provide { NavigatorImpl(get<SettingsServiceImpl>(SVC_SETTINGS_DEFAULT)) as Navigator }

            context(Scopes.CTX_MAIN_ACTIVITY) {

                provide { ResourceFinderImpl(getProperty(ACTIVITY_CONTEXT)) as ResourceFinder }
                provide { LanguageProviderImpl(get()) as LanguageProvider }

                provide(USE_CASE_NAVIGATION_WATCH) { NavigationWatchUseCase(get()) as UseCase }
                provide(USE_CASE_NAVIGATION_TO) { NavigationToUseCase(get()) as UseCase }

                factory(USE_CASE_SETTINGS_SAVE) {
                    SettingsSaveUseCase(get<SettingsServiceImpl>(SVC_SETTINGS_DEFAULT), get()) as UseCase
                }

                provide(USE_CASE_CHECK) { SetupCheckUseCase(get(SVC_SETTINGS_DEFAULT)) as UseCase }

                factory(USE_CASE_FAVORITE_WATCH) {
                    FavoriteWatchUseCase(get(SVC_SETTINGS_DEFAULT), get()) as UseCase
                }

                factory(USE_CASE_FAVORITE_CHANGE) {
                    FavoriteChangeUseCase(get()) as UseCase
                }

                provide { MainPresentation(get(USE_CASE_NAVIGATION_WATCH), get(USE_CASE_NAVIGATION_TO), get()) }

                //setup language - fragment context
                context(Scopes.CTX_SETUP_LANGUAGE_FRAGMENT) {
                    provide(USE_CASE_LANGUAGE_SETUP) { LanguageSetupUseCase(get()) as UseCase }
                    provide { LanguageSetupPresentation(get(USE_CASE_LANGUAGE_SETUP), get(USE_CASE_SETTINGS_SAVE), get()) }
                }

                context(Scopes.CTX_SETUP_TYPE_CHOOSE_FRAGMENT) {
                    provide { SetupTypeChoosePresentation(get(USE_CASE_NAVIGATION_TO), get()) }
                }

                context(Scopes.CTX_SETUP_AUTOMATIC_LOCATION) {
                    provide(USE_CASE_AUTOMATIC_SETUP) {
                        AutomaticSetupUseCase(get(), get(REPO_AUTOCOMPLETE_PROXY), get())
                    }
                    provide {
                        AutomaticSetupPresentation(
                                get(USE_CASE_AUTOMATIC_SETUP),
                                get(USE_CASE_SETTINGS_SAVE),
                                get(USE_CASE_SELECTED_AUTOCOMPLETE),
                                get(USE_CASE_NAVIGATION_TO),
                                get())
                    }
                }

                context(Scopes.CTX_SETUP_MANUAL_LOCATION) {
                    provide {
                        ManualSetupPresentation(
                                get(USE_CASE_SELECTED_AUTOCOMPLETE),
                                get(USE_CASE_SETTINGS_SAVE),
                                get(USE_CASE_NAVIGATION_TO),
                                get())
                    }
                }

                context(Scopes.CTX_AUTOCOMPLETE_SEARCH) {
                    provide {
                        AutoCompleteSearchPresentation(
                                get(USE_CASE_AUTOCOMPLETE),
                                get(USE_CASE_NAVIGATION_TO),
                                get())
                    }
                }

                context(Scopes.CTX_FORECAST) {
                    provide(USE_CASE_FORECAST_WATCH) {
                        ForecastWatchUseCase(
                                get(SVC_SETTINGS_DEFAULT),
                                get(),
                                get(),
                                get(),
                                get()) as UseCase
                    }
                    provide(USE_CASE_FORECAST_UPDATE) {
                        ForecastUpdateUseCase(
                                get(SVC_SETTINGS_DEFAULT),
                                get(),
                                get(),
                                get()) as UseCase
                    }

                    provide {
                        ForecastPresentation(
                                get(USE_CASE_FORECAST_WATCH),
                                get(USE_CASE_FORECAST_UPDATE),
                                get(USE_CASE_FAVORITE_WATCH),
                                get(USE_CASE_FAVORITE_CHANGE),
                                get(USE_CASE_SETTINGS_SAVE),
                                get(USE_CASE_NAVIGATION_TO),
                                get()
                        )
                    }
                }

                context(Scopes.CTX_FAVORITES) {
                    provide(USE_CASE_FAVORITE_FETCH) {
                        FavoritesFetchUseCase(get(SVC_SETTINGS_DEFAULT), get()) as UseCase
                    }
                    factory {
                        FavoritesPresentation(
                                get(USE_CASE_FAVORITE_WATCH),
                                get(USE_CASE_FAVORITE_FETCH),
                                get(USE_CASE_FAVORITE_CHANGE),
                                get(USE_CASE_NAVIGATION_TO),
                                get(),
                                FavoriteState())
                    }
                }
            }

        }

        val modules = listOf(autoCompleteModule, locationModule, favoriteModule, forecastModule,
                rxModule, roomModule, androidServicesModule, presentationModule)

    }


    override fun onCreate() {
        super.onCreate()

        //logging config
        Loggy.seed = if (BuildConfig.DEBUG) TimberSeed() else NoLogSeed

        //canary leak for debug mode
        if (!initCanaryLeak()) return

        //fabric
        Fabric.with(this, Crashlytics())

        //locale changer
        LocaleChanger.initialize(applicationContext,
                resources.getStringArray(R.array.array_lang_iso_codes)
                        .map { Locale(it) })
        //di
        startKoin(this, DI().modules,
                mapOf(DI.WU_API_KEY to BuildConfig.WEATHER_API_KEY)
        )
    }

    private fun initCanaryLeak(): Boolean {

        if (!BuildConfig.DEBUG) return true // no canary in prod

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return false
        }
        val excludedRefs = AndroidExcludedRefs.createAppDefaults()
                .instanceField("com.franmontiel.localechanger.LocaleChanger", "delegate")
                .build()
        refWatcher = LeakCanary.refWatcher(this)
                .excludedRefs(excludedRefs)
                .buildAndInstall()
        return true
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleChanger.onConfigurationChanged()
    }


}

object Scopes {
    const val CTX_MAIN_ACTIVITY = "ctx_main_activity"
    const val CTX_SETUP_LANGUAGE_FRAGMENT = "ctx_setup_language"
    const val CTX_SETUP_TYPE_CHOOSE_FRAGMENT = "ctx_setup_setup_type_choose_fragment"
    const val CTX_SETUP_AUTOMATIC_LOCATION = "ctx_setup_automatic_location"
    const val CTX_SETUP_MANUAL_LOCATION = "ctx_setup_manual_location"
    const val CTX_AUTOCOMPLETE_SEARCH = "ctx_autocomplete_search"
    const val CTX_FORECAST = "ctx_forecast"
    const val CTX_FAVORITES = "ctx_favorites"
    const val SCOPE_LESS = ""
}

@SuppressLint("TimberExceptionLogging")
class TimberSeed : Seed {

    init {
        Timber.plant(Timber.DebugTree())
        Timber.tag(Loggy.BASE_TAG)
    }

    override fun d(message: String, vararg args: Any) = Timber.d(message, *args)

    override fun d(t: Throwable) = Timber.d(t)

    override fun d(t: Throwable, message: String, vararg args: Any) = Timber.d(t, message, *args)

    override fun e(message: String, vararg args: Any) = Timber.e(message, *args)

    override fun e(t: Throwable) = Timber.e(t)

    override fun e(t: Throwable, message: String, vararg args: Any) = Timber.e(t, message, *args)

    override fun i(message: String, vararg args: Any) = Timber.i(message, *args)

    override fun i(t: Throwable) = Timber.i(t)

    override fun i(t: Throwable, message: String, vararg args: Any) = Timber.i(t, message, *args)

    override fun log(priority: Int, message: String, vararg args: Any) = Timber.log(priority, message, *args)

    override fun log(priority: Int, t: Throwable) = Timber.log(priority, t)

    override fun log(priority: Int, t: Throwable, message: String, vararg args: Any) =
            Timber.log(priority, t, message, *args)

    override fun tag(tag: String) {
        Timber.tag(tag)
    }

    override fun v(message: String, vararg args: Any) = Timber.v(message, *args)

    override fun v1(t: Throwable) = Timber.v(t)

    override fun w(message: String, vararg args: Any) = Timber.w(message, *args)

    override fun w(t: Throwable) = Timber.w(t)

    override fun w(t: Throwable, message: String, vararg args: Any) = Timber.w(t, message, *args)

    override fun wtf(message: String, vararg args: Any) = Timber.wtf(message, *args)

    override fun wtf(t: Throwable) = Timber.wtf(t)

    override fun wtf(t: Throwable, message: String, vararg args: Any) = Timber.wtf(t, message, *args)

}


object NoLogSeed : Seed {
    override fun d(message: String, vararg args: Any) {

    }

    override fun d(t: Throwable) {

    }

    override fun d(t: Throwable, message: String, vararg args: Any) {

    }

    override fun e(message: String, vararg args: Any) {

    }

    override fun e(t: Throwable) {

    }

    override fun e(t: Throwable, message: String, vararg args: Any) {

    }

    override fun i(message: String, vararg args: Any) {

    }

    override fun i(t: Throwable) {

    }

    override fun i(t: Throwable, message: String, vararg args: Any) {

    }

    override fun log(priority: Int, message: String, vararg args: Any) {

    }

    override fun log(priority: Int, t: Throwable) {

    }

    override fun log(priority: Int, t: Throwable, message: String, vararg args: Any) {

    }

    override fun tag(tag: String) {

    }

    override fun v(message: String, vararg args: Any) {

    }

    override fun v1(t: Throwable) {

    }

    override fun w(message: String, vararg args: Any) {

    }

    override fun w(t: Throwable) {

    }

    override fun w(t: Throwable, message: String, vararg args: Any) {

    }

    override fun wtf(message: String, vararg args: Any) {

    }

    override fun wtf(t: Throwable) {

    }

    override fun wtf(t: Throwable, message: String, vararg args: Any) {

    }

}