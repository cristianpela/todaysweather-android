package home.crsk.todaysweather.android.presentation.setup

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import com.squareup.picasso.Picasso
import home.crsk.todaysweather.android.*
import home.crsk.todaysweather.android.presentation.autocomplete.KEY_SEARCH_SELECT_LOCATION_KEY
import home.crsk.todaysweather.android.presentation.autocomplete.KEY_SEARCH_OWNER
import home.crsk.todaysweather.domain.common.entity.simpleGoogleStaticMapUrl
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screens
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.setup.SetupState
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_setup_manual_location.*
import kotlinx.android.synthetic.main.included_setup_location_map.*
import kotlinx.android.synthetic.main.included_setup_title_progress.*
import org.koin.android.ext.android.inject


/**
 * .
 * Created by Cristian Pela on 8/1/2018.
 */
class ManualSetupFragment : BaseFragment<SetupManualViewModel, SetupState>() {

    override val contextName: String = Scopes.CTX_SETUP_MANUAL_LOCATION

    private val picasso by inject<Picasso>()

    override fun onAllowViewsToPrepare() {
        textSetupTitle.setText(R.string.setup_step_title_location_manual)
        includeItemLocation.setBackgroundColor(Color.argb(200, 255, 255, 255))
        activity
        if (activity.isInLandscape()) {
            includeSetupTitle.visibility = View.GONE
            includeItemLocation.visibility = View.GONE
        }
    }

    override fun renderState(state: SetupState) {
        btnSetupNext.toggleVisibility(state.selectedLocation != null)
        state.selectedLocation?.let {
            val mapUrl = it.simpleGoogleStaticMapUrl(null)
            picasso.customLoad(mapUrl, imageLocationBigMap) {
                placeholder(R.drawable.map_placeholder).noFade()
            }
            btnSetupNext.tag = state //bind state to button's tag click
        }
    }

    override fun renderMessages(message: CommonMessage) = Unit

    override fun onArgumentsPassed(args: Bundle) {
        val locationKey = args.getString(KEY_SEARCH_SELECT_LOCATION_KEY)
        if (locationKey != null) {
            sendIntentMaybePending(Intent.SelectLocationIntent(locationKey))
        }
    }

    override fun provideIntentStreams(): List<Observable<Intent>> = listOf(RxView
            .clicks(btnFloatLocationSearch)
            .map {
                Intent.NavigationIntent(Route(
                        Screens.SCREEN_MANUAL_SETUP,
                        Screens.SCREEN_SEARCH,
                        mapOf(KEY_SEARCH_OWNER to Screens.SCREEN_MANUAL_SETUP),
                        true,
                        Route.TransitionInfo.withoutSharedElement(
                                R.anim.slide_out_down,
                                R.anim.slide_in_up)
                ))
            },
            RxView.clicks(btnSetupNext)
                    .map { btnSetupNext.tag as SetupState }
                    .switchMap {
                        rxAlert(context,
                                R.string.alert_dialog_general_title,
                                R.string.alert_dialog_general_yes_btn,
                                R.string.alert_dialog_general_no_btn,
                                R.string.alert_dialog_message_setup_location_next, it.selectedLocation!!.city)
                    }.filter { confirm -> confirm }.map {
                Intent.SaveIntent(btnSetupNext.tag)
            }).map { it.cast(Intent::class.java) }


    override fun id(): Int = R.layout.fragment_setup_manual_location

    override fun viewModelBlueprint(): Class<SetupManualViewModel> = SetupManualViewModel::class.java

    override fun screenId(): String? = Screens.SCREEN_MANUAL_SETUP

    override fun onDestroy() {
        picasso.cancelRequest(imageLocationBigMap)
        super.onDestroy()
    }
}

