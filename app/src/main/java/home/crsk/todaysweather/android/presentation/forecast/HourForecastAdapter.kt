package home.crsk.todaysweather.android.presentation.forecast

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import home.crsk.todaysweather.android.R
import home.crsk.todaysweather.android.getWeatherConditionDrawable
import home.crsk.todaysweather.domain.common.entity.HourForecast
import home.crsk.todaysweather.domain.common.other.asPercent
import home.crsk.todaysweather.domain.forecast.presentation.DisplayableHourForecast
import kotlinx.android.synthetic.main.item_forecast_hour.view.*

/**
 * .
 * Created by Cristian Pela on 02.02.2018.
 */
class HourForecastAdapter() : RecyclerView.Adapter<HourForecastVH>() {

    private val items: MutableList<DisplayableHourForecast> = mutableListOf()

    private var currentSelectedIndex: Int = RecyclerView.NO_POSITION

    fun changeItems(newItems: List<DisplayableHourForecast>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): HourForecastVH =
            LayoutInflater
                    .from(parent?.context)
                    .inflate(R.layout.item_forecast_hour, parent, false)
                    .let { HourForecastVH(it) }

    override fun getItemCount(): Int = items.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: HourForecastVH, position: Int) {
        with(items[position]) {
            holder.itemView.setBackgroundResource(
                    if (currentSelectedIndex != RecyclerView.NO_POSITION &&
                            position == currentSelectedIndex) {
                        R.drawable.round_border_background
                    } else {
                        android.R.color.transparent
                    }
            )
            holder.itemView.textHHour.text = hourDisplay
            holder.itemView.textHChance.text = hourPrecipitationChance
            holder.itemView.textHTemp.text = hourTempDisplay
            holder.itemView.imageHCondition
                    .setImageDrawable(holder.itemView.context.getWeatherConditionDrawable(hourIconUrl))
        }
    }

    fun setCurrentSelectedIndex(index: Int) {
        if (index != currentSelectedIndex) {
            currentSelectedIndex = index
            notifyDataSetChanged()
        }
    }

}

class HourForecastVH(view: View) : RecyclerView.ViewHolder(view)