package home.crsk.todaysweather.android

import home.crsk.todaysweather.domain.common.other.TranslateableError

/**
 * .
 * Created by Cristian Pela on 4/1/2018.
 */

class TranslatableIOError(
        wrapped: Throwable,
        transId: String? = null,
        params: Array<Any> = emptyArray()
): TranslateableError(wrapped, transId, params){

}