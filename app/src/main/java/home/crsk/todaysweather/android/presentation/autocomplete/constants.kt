package home.crsk.todaysweather.android.presentation.autocomplete

/**
 * .
 * Created by Cristian Pela on 16.02.2018.
 */

const val KEY_SEARCH_OWNER = "KEY_SEARCH_OWNER"
const val KEY_SEARCH_SELECT_LOCATION_KEY = "KEY_SEARCH_SELECT_LOCATION_KEY"
const val KEY_SEARCH_PARTIAL = "KEY_SEARCH_PARTIAL"