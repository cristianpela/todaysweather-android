package home.crsk.todaysweather.android

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import home.crsk.todaysweather.android.presentation.HasPresentation
import home.crsk.todaysweather.android.services.ContextAwareComponent
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.State
import org.koin.android.ext.android.releaseContext
import java.util.concurrent.TimeUnit


/**
 * .
 * Created by Cristian Pela on 20/12/2017.
 */
@Suppress("MemberVisibilityCanBePrivate")
abstract class BaseFragment<VM : ViewModel, in S : State>()
    : Fragment(), IntentRenderer<S>, ContextAwareComponent {

    private lateinit var viewModel: VM

    protected lateinit var supportActivity: AppCompatActivity

    protected val screenSize by lazy {
        activity.getScreenSize()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        supportActivity = context as AppCompatActivity
    }

    private val hasPresentationHandler = HasPresentationHandler<S>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity).get(viewModelBlueprint())
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(id(), container, false)
    }


    @CallSuper
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) =
            if (viewModel is HasPresentation<*>) {
                val firstTime = savedInstanceState == null
                hasPresentationHandler.updateBundles(savedInstanceState, if (firstTime) arguments else null)
                hasPresentationHandler.allowRendererToPrepareViews(this)
                hasPresentationHandler.subscribeStreams(viewModel as HasPresentation<*>, this)
            } else {
                throw IllegalStateException("Your ViewModel must implement HasPresentation")
            }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroyView() {
        hasPresentationHandler.disposeStreams(this)
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putAll(hasPresentationHandler.getLaterSavingInstance())
        super.onSaveInstanceState(outState)
    }


    @CallSuper
    override fun onDestroy() {
        if (contextName != Scopes.SCOPE_LESS)
            releaseContext(contextName) // release koin context
        super.onDestroy()
        TodaysWeatherApplication.getRefWatcher(context)?.watch(this)
    }

    override fun renderErrors(err: Throwable) {
        Snackbar.make(view!!, err.message.toString(), Snackbar.LENGTH_LONG)
                .multiLines()
                .show()
    }

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) = Unit

    override fun onArgumentsPassed(args: Bundle) {}

    fun addToLaterSavingInstance(key: String, value: Any?) = hasPresentationHandler
            .addToLaterSavingInstance(key, value)

    fun removeFromLaterFromSavingInstance(key: String) = hasPresentationHandler
            .removeFromLaterFromSavingInstance(key)

    fun getFromSavingInstance(key: String): Any? = hasPresentationHandler.getLaterSavingInstance()[key]

    fun sendIntentMaybePending(intent: Intent) = hasPresentationHandler.sendIntentMaybePending(intent)


    fun post(action: () -> Unit) = hasPresentationHandler.post(action)

    fun postDelayed(value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS, action: () -> Unit): Long =
            hasPresentationHandler.postDelayed(value, unit, action)


    fun cancelAndDelay(token: Long? = null, value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS, action: () -> Unit): Long {
        clearPostAction(token)
        return hasPresentationHandler.postDelayed(value, unit, action)
    }

    fun clearPostAction(token: Long?) {
        if (token != null)
            hasPresentationHandler.clearPostAction(token)
    }

    fun clearAllPostActions() {
        hasPresentationHandler.clearAllPostActions()
    }

    fun delayedToggleVisibility(prevToken: Long?, state: State, view: View,
                                value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS): Long? =
            if (state.inFlight) {
                cancelAndDelay(prevToken, value, unit) {
                    view.toggleVisibility(true)
                }
            } else {
                clearPostAction(prevToken)
                view.toggleVisibility(false)
                null
            }


    @LayoutRes
    abstract fun id(): Int

    abstract fun viewModelBlueprint(): Class<VM>

    open fun screenId(): String? = null


}