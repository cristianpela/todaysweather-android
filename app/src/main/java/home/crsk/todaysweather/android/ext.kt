@file:Suppress("unused")

package home.crsk.todaysweather.android

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.*
import android.support.annotation.IntRange
import android.support.design.widget.Snackbar
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.View.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.log.Loggy
import io.reactivex.Observable
import java.text.Normalizer

        /**
         * .
         * Created by Cristian Pela on 30/12/2017.
         */

typealias AndroidIntent = android.content.Intent

fun toastIt(context: Context, message: String, where: Int = Gravity.BOTTOM) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).apply {
        setGravity(where, 0, 0)
    }.show()
}

fun String.stripDiacritics() = java.lang.String(Normalizer
        .normalize(this, Normalizer.Form.NFD))
        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")!!

fun Activity.isInLandscape(): Boolean = resources.configuration.orientation.let {
    it == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || it == ActivityInfo.SCREEN_ORIENTATION_USER
}

fun Activity.getScreenSize(): Pair<Int, Int> {
    val dm = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(dm)
    //get pixels and covert them to dp
    return dm.widthPixels to dm.heightPixels
}

fun Int.toDp(displayMetrics: DisplayMetrics = DisplayMetrics()) = TypedValue
        .applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                this.toFloat(), displayMetrics).toInt()

fun View.resize(width: Int, height: Int) {
    layoutParams.width = width
    layoutParams.height = height
    setLayoutParams(layoutParams)
}


fun rxAlert(context: Context,
            @StringRes title: Int,
            @StringRes positiveButton: Int = android.R.string.ok,
            @StringRes cancelButton: Int = android.R.string.cancel,
            @StringRes message: Int,
            vararg messageArgs: Any
): Observable<Boolean> =
        Observable.create { emitter ->
            val dialog = AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(context.getString(message, *messageArgs))
                    .setPositiveButton(positiveButton) { _, _ ->
                        emitter.onNext(true)
                        emitter.onComplete()
                    }
                    .setNegativeButton(cancelButton) { _, _ ->
                        emitter.onNext(false)
                        emitter.onComplete()
                    }
                    .setOnDismissListener {
                        emitter.onNext(false)
                        emitter.onComplete()
                    }
                    .create()
            emitter.setCancellable { dialog.dismiss() }
            dialog.show()
        }

fun LanguageProvider.Companion.ext() {
    val ERR_LOCATION_PROVIDER_UNAVAILABLE_OR_NO_INTERNET = "err_location_provider_unavailable_or_no_internet"
}

fun Map<String, Any>.toBundle(): Bundle {
    fun addToBundle(entry: Map.Entry<String, Any>, bundle: Bundle): Unit =
            //todo expand types accepted by bundle
            when (entry.value) {
                is Boolean -> bundle.putBoolean(entry.key, entry.value as Boolean)
                is String -> bundle.putString(entry.key, entry.value as String)
                is Int -> bundle.putInt(entry.key, entry.value as Int)
                is Float -> bundle.putFloat(entry.key, entry.value as Float)
                is Double -> bundle.putDouble(entry.key, entry.value as Double)
                else -> throw Exception("Unsupported for ${entry.value.javaClass} ")
            }

    val bundle = Bundle()
    this.entries.forEach { addToBundle(it, bundle) }
    return bundle
}

fun RecyclerView.setupVerticalWithAdapter(adapter: RecyclerView.Adapter<*>) {
    this.adapter = adapter
    layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL))
}


fun View.toggleVisibility(visible: Boolean, visibilityType: Int = GONE) {
    when (visibilityType) {
        GONE -> visibility = if (visible) VISIBLE else GONE
        INVISIBLE -> visibility = if (visible) VISIBLE else INVISIBLE
    }
}

fun autoCompleteTransition(ctx: Context): Route.TransitionInfo? =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Route.TransitionInfo(
                    R.string.transition_name_autocomplete_search,
                    R.id.toolbarAutocomplete,
                    android.R.transition.move,
                    android.R.transition.fade,
                    android.R.transition.fade
            )
        } else {
            null
        }

fun Snackbar.multiLines(@IntRange(from = 3) lines: Int = 3): Snackbar {
    view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
            .maxLines = lines
    return this
}


fun Context.getColorCompat(@ColorRes id: Int) = ContextCompat.getColor(this, id)

fun String.extractFileNameFromPath(includeExtension: Boolean = false): String? {
    return (lastIndexOf('/') to if (includeExtension) length else lastIndexOf('.'))
            .let {
                if (it.first != -1)
                    substring(it.first + 1, if (it.second == -1) length else it.second)
                else
                    null
            }
}

fun Context.getWeatherConditionDrawable(iconUrl: String): Drawable {
    val iconName = iconUrl.extractFileNameFromPath()?.let { "ic_wu_${it}" }
    return try {
        val resDrawable = R.drawable::class.java.getField(iconName).getInt(null)
        ContextCompat.getDrawable(this, resDrawable)
    } catch (ex: Exception) {
        ContextCompat.getDrawable(this, R.drawable.wu_placeholder)
    }
}


fun View.initLoadingState() {
    // Loggy.d("View Loading State Init 0")
    this.setTag(R.id.TAG_PROGRESSBAR_LOADING_STATE, 0)
}

inline fun <V : View> V.pushLoadingState(action: V.() -> Unit = { visibility = View.VISIBLE }) {
    getTag(R.id.TAG_PROGRESSBAR_LOADING_STATE)?.let {
        this.setTag(R.id.TAG_PROGRESSBAR_LOADING_STATE, (it as Int) + 1)
        //Loggy.d("View Loading State After Push ${(getTag(R.id.TAG_PROGRESSBAR_LOADING_STATE) as Int)}")
        this.action()
    }
}

inline fun <V : View> V.popLoadingState(action: V.() -> Unit = { visibility = View.GONE }) {
    getTag(R.id.TAG_PROGRESSBAR_LOADING_STATE)?.let {
        val popLoadState = (it as Int) - 1
        if (popLoadState > 0) {
            this.setTag(R.id.TAG_PROGRESSBAR_LOADING_STATE, popLoadState)
            //  Loggy.d("View Loading State After Pop ${(getTag(R.id.TAG_PROGRESSBAR_LOADING_STATE) as Int)}")
        } else {
            //  Loggy.d("View Loading State Pop Reached Initial State ${(it as Int)}. Execute Action")
            initLoadingState()
            this.action()
        }
    }
}

fun Drawable.colorFilter(context: Context, @ColorRes colorRes: Int = R.color.colorAccent,
                         mode: PorterDuff.Mode = PorterDuff.Mode.SRC_ATOP) =
        setColorFilter(context.getColorCompat(colorRes), mode)

/**
 * enables feats that one would activate them in debug mode as logging or loading source ribbon indicator
 */
fun Picasso.Builder.enableDebugFeats(): Picasso.Builder =
        if (BuildConfig.DEBUG) this.loggingEnabled(true).indicatorsEnabled(true) else this


inline fun Picasso.customLoad(url: String,
                              imageView: ImageView,
                              callback: Callback? = null,
                              crossinline extras: RequestCreator .() -> RequestCreator = { this }) {
    load(url).error(android.R.drawable.stat_notify_error)
            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
            .fit()
            .centerCrop()
            .placeholder(R.drawable.map_placeholder)
            .extras()
            .into(imageView, callback)
}

fun <T> T.toSingletonList() = listOf<T>(this)

fun Fragment.isDialog() = this is DialogFragment

inline fun <reified T : View> Toolbar.actionView(@IdRes id: Int, action: T.() -> Unit = {}): T{
    val view = menu.findItem(id).actionView as T
    view.apply(action)
    return view
}