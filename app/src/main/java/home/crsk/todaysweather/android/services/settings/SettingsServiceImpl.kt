package home.crsk.todaysweather.android.services.settings

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.google.gson.*
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.setup.usecases.NoSetupError
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

/**
 * .
 * Created by Cristian Pela on 14/12/2017.
 */
class SettingsServiceImpl(private val preferences: SharedPreferences) : SettingsService {

    private val gson: Gson = GsonBuilder()
            .serializeNulls()
            .registerTypeAdapter(UnitEnum::class.java, JsonSerializer<UnitEnum> { src, _, _ ->
                JsonPrimitive(src.ordinal)
            })
            .registerTypeAdapter(UnitEnum::class.java, JsonDeserializer<UnitEnum> { json, _, _ ->
                UnitEnum.values()[json.asInt]
            })
            .create()

    companion object {
        private const val KEY_SETTINGS = "key_settings"
        private const val DEFAULT_JSON_SETTINGS = """
            {
                "locationKey": null,
                "langCodeISO": null,
                "unit":0
            }
            """
    }

    override fun check(): Completable = Completable.create {
        val settings = getSettings()
        if (settings.isSet()) {
            it.onComplete()
        } else {
            it.onError(NoSetupError)
        }
    }

    override fun current(): Single<Settings> = Single.defer { Single.just(getSettings()) }

    override fun observe(): Observable<Settings> = Observable.create<Settings> { emitter ->
        emitter.onNext(getSettings()) // emit initial settings regardless of their existence
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (key == KEY_SETTINGS) {
                emitter.onNext(getSettings())
            }
        }
        preferences.registerOnSharedPreferenceChangeListener(listener)
        emitter.setCancellable { preferences.unregisterOnSharedPreferenceChangeListener(listener) }
    }.share()

    override fun save(settings: Settings): Completable = Completable.create {
        saveSettings(settings)
        it.onComplete()
    }

    private fun getSettings(): Settings = gson.fromJson(
            preferences.getString(KEY_SETTINGS, DEFAULT_JSON_SETTINGS), Settings::class.java)

    private fun saveSettings(settings: Settings) {
        val json = gson.toJson(settings)
        preferences.edit().putString(KEY_SETTINGS, json).apply()
    }
}