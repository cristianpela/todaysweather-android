package home.crsk.todaysweather.android.presentation.forecast

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import home.crsk.todaysweather.android.R
import home.crsk.todaysweather.android.getWeatherConditionDrawable
import home.crsk.todaysweather.domain.forecast.presentation.NextDaySummary
import kotlinx.android.synthetic.main.item_forecast_next_day_sumary.view.*

/**
 * .
 * Created by Cristian Pela on 08.02.2018.
 */
class NextDaysSummaryAdapter() : RecyclerView.Adapter<NextDaySummaryVH>() {

    private val items: MutableList<NextDaySummary> = mutableListOf()

    fun changeItems(newItems: List<NextDaySummary>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NextDaySummaryVH =
            LayoutInflater
                    .from(parent?.context)
                    .inflate(R.layout.item_forecast_next_day_sumary, parent, false)
                    .let { NextDaySummaryVH(it) }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: NextDaySummaryVH, position: Int) {
        with(items[position]) {
            holder.itemView.textNextDaySumDate.text = date
            holder.itemView.textNextDaySumMinMaxTemp.text = tempMinMaxDisplay
            holder.itemView.imageNextDaySumIcon.setImageDrawable(holder.itemView
                    .context.getWeatherConditionDrawable(iconUrl))
        }
    }
}


class NextDaySummaryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

}