package home.crsk.todaysweather.android.presentation.setup

import com.jakewharton.rxbinding2.view.RxView
import home.crsk.todaysweather.android.BaseFragment
import home.crsk.todaysweather.android.R
import home.crsk.todaysweather.android.Scopes.CTX_SETUP_TYPE_CHOOSE_FRAGMENT
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screens
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.NoState
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_setup_choose_type.*
import kotlinx.android.synthetic.main.included_setup_title_progress.*

/**
 * .
 * Created by Cristian Pela on 24/12/2017.
 */
class SetupTypeChooseFragment : BaseFragment<SetupTypeChooseViewModel, NoState>() {

    override val contextName: String = CTX_SETUP_TYPE_CHOOSE_FRAGMENT

    override fun renderState(state: NoState) = Unit

    override fun renderMessages(message: CommonMessage) = Unit

    override fun renderErrors(err: Throwable) = Unit

    override fun onAllowViewsToPrepare() {
        textSetupTitle.setText(R.string.setup_step_title_choose_type)
    }

    override fun provideIntentStreams(): List<Observable<Intent>> = listOf(
            RxView.clicks(btnSetupAuto)
                    .map {
                        Intent.NavigationIntent(Route(
                                Screens.SCREEN_CHOOSE_SETUP_TYPE,
                                Screens.SCREEN_AUTOMATIC_SETUP))
                    }
                    .cast(Intent::class.java),
            RxView.clicks(btnSetupManual)
                    .map {
                        Intent.NavigationIntent(Route(
                                Screens.SCREEN_CHOOSE_SETUP_TYPE,
                                Screens.SCREEN_MANUAL_SETUP))
                    }
                    .cast(Intent::class.java)
    )

    override fun id(): Int = R.layout.fragment_setup_choose_type

    override fun viewModelBlueprint(): Class<SetupTypeChooseViewModel> = SetupTypeChooseViewModel::class.java
}