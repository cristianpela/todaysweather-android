package home.crsk.todaysweather.android.presentation.autocomplete

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import home.crsk.todaysweather.android.R
import home.crsk.todaysweather.domain.common.entity.Location
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.item_location.view.*


/**
 * .
 * Created by Cristian Pela on 12/1/2018.
 */
class AutoCompleteSearchAdapter : RecyclerView.Adapter<LocationVH>() {

    private val locations: MutableList<OwnerLocation> = mutableListOf()

    private val selectSubject: Subject<OwnerLocation> = PublishSubject.create()

    fun observeSelection(): Observable<OwnerLocation> = selectSubject

    fun setLocations(owner: String?, diffResultWrap: Pair<DiffUtil.DiffResult, List<Location>>) {
        locations.clear()
        locations.addAll(diffResultWrap.second.map { owner to it })
        diffResultWrap.first.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: LocationVH, position: Int) {
        val location = locations[position].second
        holder.view.txtLocationName.text = "${location.city}, ${location.countryCode}"
        holder.view.txtLocationLat.text = location.latitude.toString()
        holder.view.txtLocationLong.text = location.longitude.toString()
    }

    override fun getItemCount(): Int = locations.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationVH {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_location, parent, false)
        return LocationVH(view, locations, selectSubject, parent)
    }

}

class LocationDiffCallback(
        private val oldItems: List<Location>,
        private val newItems: List<Location>
) : DiffUtil.Callback() {

    companion object {
        val EMPTY_DIFF_RESULT = DiffUtil.calculateDiff(LocationDiffCallback(emptyList(), emptyList()))
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition].locationKey == newItems[newItemPosition].locationKey

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            (oldItems[oldItemPosition] == newItems[newItemPosition])

}

class LocationVH(val view: View, locations: List<OwnerLocation>, selectSubject: Subject<OwnerLocation>, parent: ViewGroup) :
        RecyclerView.ViewHolder(view) {
    init {
        RxView.clicks(view)
                .takeUntil(RxView.detaches(parent))
                .map { locations[adapterPosition] }
                .subscribe(selectSubject)
    }
}


typealias OwnerLocation = Pair<String?, Location>