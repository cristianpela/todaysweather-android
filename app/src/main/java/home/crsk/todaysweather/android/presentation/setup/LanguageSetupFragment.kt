package home.crsk.todaysweather.android.presentation.setup

import android.app.Activity
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.franmontiel.localechanger.LocaleChanger
import com.franmontiel.localechanger.utils.ActivityRecreationHelper
import com.jakewharton.rxbinding2.view.RxView
import home.crsk.todaysweather.android.*
import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.setup.presentation.LanguageSetupState
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_setup_language.*
import kotlinx.android.synthetic.main.included_setup_title_progress.*
import kotlinx.android.synthetic.main.item_recycler_languages.view.*
import java.util.*

/**
 * .
 * Created by Cristian Pela on 20/12/2017.
 */
class LanguageSetupFragment : BaseFragment<LanguageSetupViewModel, LanguageSetupState>() {

    override val contextName: String = Scopes.CTX_SETUP_LANGUAGE_FRAGMENT

    override fun onAllowViewsToPrepare() {
        progressSetup.indeterminateDrawable
                .setColorFilter(context.getColorCompat(R.color.colorAccent), PorterDuff.Mode.SRC_IN)
        textSetupTitle.text = context.getString(R.string.setup_step_title_language)
        recyclerLanguages.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerLanguages.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL))
    }

    override fun renderState(state: LanguageSetupState) {
        progressSetup.visibility = if (state.inFlight) View.VISIBLE else View.INVISIBLE
        btnSetupNext.visibility = if (state.inFlight) View.INVISIBLE else View.VISIBLE
        recyclerLanguages.adapter = RecyclerLanguagesAdapter(state.languages, activity)
    }


    override fun renderMessages(message: CommonMessage) {

    }

    override fun renderErrors(err: Throwable) {

    }

    override fun provideIntentStreams(): List<Observable<Intent>> = listOf(
            RxView.clicks(btnSetupNext)
                    .switchMap {
                        rxAlert(context,
                                R.string.alert_dialog_general_title,
                                R.string.alert_dialog_general_yes_btn,
                                R.string.alert_dialog_general_no_btn,
                                R.string.alert_dialog_message_setup_language_next)
                    }
                    .filter { confirm -> confirm }
                    .map { Intent.SaveIntent(Locale.getDefault().language) }
                    .cast(Intent::class.java)
    )

    override fun id(): Int = R.layout.fragment_setup_language

    override fun viewModelBlueprint(): Class<LanguageSetupViewModel> = LanguageSetupViewModel::class.java

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) {
        intentSender.sendIntent(Intent.InitIntent<Unit>())
    }

    override fun onDestroyView() {
        (recyclerLanguages.adapter as RecyclerLanguagesAdapter).parentActivity = null
        super.onDestroyView()
    }

}

//todo find a better way to restart activity - current aproach looks dirty
class RecyclerLanguagesAdapter(private val languages: List<Language>, var parentActivity: Activity?) : RecyclerView.Adapter<LanguageItemVH>() {

    override fun getItemCount(): Int = languages.size

    override fun onBindViewHolder(holder: LanguageItemVH, position: Int) {
        val language = languages[position]
        //todo is not working is not translated, have to do it manually and so no more lose coupling
        holder.view.itemTxtLangName.text = when (language.isoCode) {
            "ro" -> holder.view.context.getString(R.string.lang_name_ro)
            "en" -> holder.view.context.getString(R.string.lang_name_en)
            else -> ""
        }
        holder.view.itemImgFlag.setImageDrawable(ContextCompat
                .getDrawable(holder.view.context, language.flagImage))
        holder.view.isSelected = language.selected
        holder.view.setOnClickListener { view ->
            if (!view.isSelected) {
                LocaleChanger.setLocale(Locale(languages[holder.adapterPosition].isoCode))
                ActivityRecreationHelper.recreate(parentActivity, false)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageItemVH {
        return LanguageItemVH(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_recycler_languages, parent, false))
    }


}


class LanguageItemVH(val view: View) : RecyclerView.ViewHolder(view)


