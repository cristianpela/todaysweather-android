package home.crsk.todaysweather.android

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.*
import android.support.v4.content.ContextCompat
import android.transition.TransitionInflater
import android.view.View
import home.crsk.todaysweather.android.presentation.autocomplete.AutoCompleteSearchFragment
import home.crsk.todaysweather.android.presentation.forecast.ForecastFragment
import home.crsk.todaysweather.android.presentation.setup.*
import home.crsk.todaysweather.domain.common.gateway.*
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.main.NavigationState
import io.reactivex.Observable
import org.koin.android.ext.android.inject
import java.lang.UnsupportedOperationException


class MainActivity : BaseActivity<MainViewModel, NavigationState>(), NavigationInterceptor {

    override val contextName: String = Scopes.CTX_MAIN_ACTIVITY

    //to avoid - "Can request only one set of permissions at a time"
    //that cause permission dialog to not pop, we send only one permission, FINE_LOCATION
    //which super-seed COARSE
    private val permissions = arrayOf(
//            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION)

    private val permissionRequestCodeHandle = 137

    //todo: sloppy code. navigator should not be here... but couldn't find a better solution
    private val navigator by inject<Navigator>()

    private val screenFragmentManager = ScreenFragmentManager(supportFragmentManager)

    override fun onNavigationIntercept(navChain: NavChain): Route =
            //might not be on ui thread
            if (navChain.route.to == Screens.SCREEN_AUTOMATIC_SETUP) {
                if (!arePermissionsGranted()) {
                    ActivityCompat.requestPermissions(this, permissions, permissionRequestCodeHandle)
                    navChain.proceed(navChain.route.reverse())
                } else {
                    navChain.proceed(navChain.route)
                }
            } else {
                navChain.proceed(navChain.route)
            }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == permissionRequestCodeHandle) {
            if (grantResults.isNotEmpty()) {
                val granted = grantResults
                        .map { it == PackageManager.PERMISSION_GRANTED }
                        .reduce { acc, gr -> acc && gr }
                val route = Route(
                        Screens.SCREEN_CHOOSE_SETUP_TYPE,
                        Screens.SCREEN_AUTOMATIC_SETUP
                )
                sendIntentMaybePending(Intent.NavigationIntent(if (granted) route else route.reverse()))
            }
        }
    }

    private fun arePermissionsGranted(): Boolean = permissions.map {
        ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }.reduce { acc, granted -> acc && granted }

    override fun id(): Int = R.layout.activity_main

    override fun viewModelBlueprint(): Class<MainViewModel> = MainViewModel::class.java

    override fun onAllowViewsToPrepare() = Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        navigator.addInterceptor(this)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        navigator.removeInterceptor(this)
        super.onDestroy()
    }

    override fun renderState(state: NavigationState) = with(state) {
        if (this.route != Route.NONE) {
            showScreen(route)
        }
    }

    override fun renderMessages(message: CommonMessage) {}

    override fun renderErrors(err: Throwable) {}

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) {
        intentSender.sendIntent(Intent.NavigationWatchIntent)
    }

    override fun provideIntentStreams(): List<Observable<Intent>> = emptyList()

    private fun showScreen(route: Route) {
        screenFragmentManager.showScreen(route)
    }



}

