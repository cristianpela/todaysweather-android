package home.crsk.todaysweather.android

import android.os.Bundle
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.State
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import io.reactivex.Observable

/**
 * .
 * Created by Cristian Pela on 19/12/2017.
 */
interface IntentRenderer<in S : State> {
    fun renderState(state: S)
    fun renderMessages(message: CommonMessage)
    fun renderErrors(err: Throwable)
    fun provideIntentStreams(): List<Observable<Intent>>
    fun onAllowViewsToPrepare()
    fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle)
    fun onArgumentsPassed(args: Bundle)
}