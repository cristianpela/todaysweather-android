package home.crsk.todaysweather.android.services.rx

import home.crsk.todaysweather.domain.common.other.SchedulersContract
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * .
 * Created by Cristian Pela on 11/12/2017.
 */
class SchedulersContractImpl : SchedulersContract {

    override fun computation(): Scheduler = Schedulers.computation()

    override fun io(): Scheduler = Schedulers.io()

    override fun main(): Scheduler = AndroidSchedulers.mainThread()

}