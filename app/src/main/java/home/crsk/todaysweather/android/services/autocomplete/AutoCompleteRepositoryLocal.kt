package home.crsk.todaysweather.android.services.autocomplete

import home.crsk.todaysweather.android.dataroom.location.LocationDAO
import home.crsk.todaysweather.android.services.room.toDatabaseEntity
import home.crsk.todaysweather.android.services.room.toDomainEntity
import home.crsk.todaysweather.android.services.room.wildcardStartWith
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 *
 * Created by Cristian Pela on 11/12/2017.
 */
class AutoCompleteRepositoryLocal(private val dao: LocationDAO) : AutoCompleteRepository {

    override fun findByKey(key: String): Maybe<Location> =
            Maybe.defer {
                dao.findByKey(key) }.map { it.toDomainEntity() }

    override fun save(locations: List<Location>): Single<List<Location>> =
            Completable.create {
                dao.save(locations.map { it.toDatabaseEntity() })
                it.onComplete()
            }.andThen(Single.just(locations))

    override fun find(partialSearch: String): Maybe<List<Location>> =
            Maybe.defer {
                dao.find(partialSearch.wildcardStartWith())}
                    .filter { it.isNotEmpty() }
                    .map { it.map { it.toDomainEntity() } }

    override fun findExact(city: String, country: String): Maybe<List<Location>> =
            Maybe.defer {
                dao.find(city.wildcardStartWith()) }
                    .filter { it.isNotEmpty() }
                    .map { it.map { it.toDomainEntity() } }
}