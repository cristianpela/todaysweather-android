package home.crsk.todaysweather.android.presentation

import home.crsk.todaysweather.domain.common.presentation.PresentationStreamOps

/**
 * .
 * Created by Cristian Pela on 19/12/2017.
 */
interface HasPresentation<P : PresentationStreamOps<*>> {
    val presentation: P
}