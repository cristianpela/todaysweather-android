package home.crsk.todaysweather.android.presentation.favorites

import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.helper.ItemTouchHelper.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import home.crsk.todaysweather.android.*
import home.crsk.todaysweather.android.presentation.autocomplete.KEY_SEARCH_SELECT_LOCATION_KEY
import home.crsk.todaysweather.android.presentation.autocomplete.LocationDiffCallback
import home.crsk.todaysweather.android.presentation.setup.BaseViewModel
import home.crsk.todaysweather.domain.common.entity.Favorite
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.asPercent
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.favorites.FavoriteState
import home.crsk.todaysweather.domain.favorites.presentation.DisplayableFavorite
import home.crsk.todaysweather.domain.favorites.presentation.FavoritesPresentation
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.fragment_favorites.*
import kotlinx.android.synthetic.main.fragment_forecast.*
import kotlinx.android.synthetic.main.item_favorite.view.*
import org.koin.standalone.inject

/**
 * .
 * Created by Cristian Pela on 19.02.2018.
 */
class FavoritesFragment : BaseDialogFragment<FavoritesViewModel, FavoriteState>() {

    private val adapter = FavoritesAdapter()

    init {
        setFullScreen = true
    }

    override fun onAllowViewsToPrepare() {
        with(recyclerFavorites) {
            adapter = this@FavoritesFragment.adapter
            addItemDecoration(
                    EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL))
            ItemTouchHelper(FavoriteRemoveTouchCallback(this@FavoritesFragment.adapter))
                    .attachToRecyclerView(this)
        }

        containerFavorites.setOnTouchListener { _, _ ->
            sendIntentMaybePending(Intent.NavigationIntent(Route.back(emptyMap())))
            true
        }
    }

    override val contextName: String
        get() = Scopes.CTX_FAVORITES

    override fun renderState(state: FavoriteState) {
        if(state.hasUndoHistory)
            fabUndoFavorite.show()
        else
            fabUndoFavorite.hide()
        adapter.updateItems(state.favorites)
    }

    override fun onAllowSendIntent(intentSender: IntentSender, savedInstanceState: Bundle) {
        intentSender.sendIntent(Intent.FavoriteWatchIntent)
        intentSender.sendIntent(Intent.FavoritesFetchIntent)
    }

    override fun renderMessages(message: CommonMessage) = Unit

    override fun provideIntentStreams(): List<Observable<Intent>> =
            adapter.observe().toSingletonList() +
                    RxView.clicks(fabUndoFavorite)
                            .map { Intent.FavoriteChangeIntent(Location.NO_RESULT, true)}
                            .cast(Intent::class.java)

    override fun id(): Int = R.layout.fragment_favorites

    override fun viewModelBlueprint(): Class<FavoritesViewModel> = FavoritesViewModel::class.java
}


class FavoritesViewModel : BaseViewModel<FavoriteState>() {

    override val presentation by inject<FavoritesPresentation>()

}

class FavoritesAdapter : RecyclerView.Adapter<FavoritesVH>() {

    private val items = mutableListOf<DisplayableFavorite>()

    private val subject: Subject<Intent> = PublishSubject.create()

    fun updateItems(newItems: List<DisplayableFavorite>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun observe() = subject

    fun removeItem(position: Int) {
        val favorite = items[position]
        items.removeAt(position)
        notifyItemRemoved(position)
        subject.onNext(Intent.FavoriteChangeIntent(
                Location(locationKey = favorite.locationKey, favorite = favorite.isFavorite), true))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesVH =
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_favorite, parent, false).let {
                FavoritesVH(it, items, subject, parent)
            }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FavoritesVH, position: Int) {
        with(items[position]) {
            holder.itemView.apply {
                textFavLocation.text = city
                textFavCondition.text = condition
                imageFavConditionIcon.setImageDrawable(context.getWeatherConditionDrawable(conditionIcon))
                textFavTemp.text = temperature
                textFavTempSymbol.text = symbol
                textFavHour.text = hour
                textFavPrepChance.text = precipitationChance
            }
        }
    }

}


class FavoritesVH(val view: View,
                  items: List<DisplayableFavorite>,
                  selectSubject: Subject<Intent>,
                  parent: ViewGroup) : RecyclerView.ViewHolder(view) {
    init {
        RxView.clicks(view)
                .takeUntil(RxView.detaches(parent))
                .map { items[adapterPosition] }
                .subscribe { loc ->
                    selectSubject.onNext(
                            Intent.NavigationIntent(Route
                                    .back(mapOf(KEY_SEARCH_SELECT_LOCATION_KEY to loc.locationKey))))
                }
    }
}


class FavoriteRemoveTouchCallback(private val adapter: FavoritesAdapter) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int =
            makeFlag(ACTION_STATE_IDLE, UP) or makeFlag(ACTION_STATE_SWIPE, UP or DOWN)


    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean =
            false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        adapter.removeItem(viewHolder.adapterPosition)
    }

}