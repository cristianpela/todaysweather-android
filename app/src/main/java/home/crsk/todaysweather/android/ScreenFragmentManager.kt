package home.crsk.todaysweather.android

import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN
import home.crsk.todaysweather.android.presentation.autocomplete.AutoCompleteSearchFragment
import home.crsk.todaysweather.android.presentation.favorites.FavoritesFragment
import home.crsk.todaysweather.android.presentation.forecast.ForecastFragment
import home.crsk.todaysweather.android.presentation.setup.AutomaticSetupFragment
import home.crsk.todaysweather.android.presentation.setup.LanguageSetupFragment
import home.crsk.todaysweather.android.presentation.setup.ManualSetupFragment
import home.crsk.todaysweather.android.presentation.setup.SetupTypeChooseFragment
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screen
import home.crsk.todaysweather.domain.common.gateway.Screens
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_BACK
import home.crsk.todaysweather.domain.common.log.Loggy
import java.lang.UnsupportedOperationException

/**
 * .
 * Created by Cristian Pela on 18.02.2018.
 */
class ScreenFragmentManager(private val fragmentManager: FragmentManager) {

    fun showScreen(route: Route) {
        Loggy.d("Route : $route")
        with(fragmentManager) {
            if (route.to == SCREEN_BACK) {
                if (backStackEntryCount > 0) {
                    if (fragments.last().isDialog()) {
                        (fragments.last() as DialogFragment).dismiss()
                        route.extras?.let {
                            (findFragmentById(R.id.container) as IntentRenderer<*>)
                                    .onArgumentsPassed(it.toBundle())
                        }
                    } else {
                        var listener: FragmentManager.FragmentLifecycleCallbacks? = null
                        //in order to send the arguments extras to the "underneath" fragment
                        // during popping back stack, we need to add a listener to its attached lifecycle phase
                        route.extras?.let {
                            listener = object : FragmentManager.FragmentLifecycleCallbacks() {
                                override fun onFragmentResumed(fm: FragmentManager?, f: Fragment?) {
                                    (f as IntentRenderer<*>).onArgumentsPassed(it.toBundle())
                                }
                            }
                            registerFragmentLifecycleCallbacks(listener, false)
                            popBackStackImmediate() //we use immediate cause we need this to be synchronous
                            //we remove the listener if any, we only need it in this "pop" time span
                            listener?.let { unregisterFragmentLifecycleCallbacks(it) }
                        }
                    }

                }
                return@with
            }
            val findFragmentByTag = findFragmentByTag(route.to)
            if (findFragmentByTag == null) {
                val toFragment = createScreen(route.to).apply {
                    arguments = route.extras?.toBundle()
                }
                if (toFragment.isDialog()) {
                    (toFragment as DialogFragment).let {
                        val transaction = beginTransaction()
                        if (route.addToBackStack) {
                            transaction.addToBackStack(route.to)
                        }
                        it.show(transaction, route.to)
                    }

                } else {
                    beginTransaction()
                            .replace(R.id.container, toFragment, route.to)
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .let {
                                if (route.addToBackStack) it.addToBackStack(null) else it
                            }
                            .setTransition(TRANSIT_FRAGMENT_OPEN)
                            .commitNow()
                }
            }
        }

    }

    private fun createScreen(screen: Screen): Fragment =
            when (screen) {
                Screens.SCREEN_LANGUAGE_SETUP -> LanguageSetupFragment()
                Screens.SCREEN_CHOOSE_SETUP_TYPE -> SetupTypeChooseFragment()
                Screens.SCREEN_AUTOMATIC_SETUP -> AutomaticSetupFragment()
                Screens.SCREEN_MANUAL_SETUP -> ManualSetupFragment()
                Screens.SCREEN_SEARCH -> AutoCompleteSearchFragment()
                Screens.SCREEN_APP -> ForecastFragment()
                Screens.SCREEN_FAVORITES -> FavoritesFragment()
                else -> throw UnsupportedOperationException("Unknown screen")
            }


}


//                val transitionInfo = route.transitionInfo
//                if (transitionInfo != null) {
//                    if (transitionInfo.isWithoutSharedElement()) {
//                        beginTransaction()
//                                .replace(R.id.container, toFragment, route.to)
//                                .setCustomAnimations(transitionInfo.toEnterTransitionId!!, transitionInfo.fromExitTransitionId!!)
//                                .let {
//                                    if (route.addToBackStack) it.addToBackStack(null) else it
//                                }.commit()
//                    } else {
//                        val fromFragment = findFragmentByTag(route.from)
//                        if (fromFragment != null && fromFragment.javaClass != toFragment.javaClass) {
//                            val commonView: View? = fromFragment.view?.findViewById(transitionInfo.commonViewId!!)
//                            val toEnterTransitionId = transitionInfo.toEnterTransitionId
//                            val fromExitTransitionId = transitionInfo.fromExitTransitionId
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                                with(TransitionInflater.from(this@MainActivity)) {
//                                    if (fromExitTransitionId != null)
//                                        fromFragment.exitTransition = inflateTransition(fromExitTransitionId)
//                                    if (toEnterTransitionId != null)
//                                        toFragment.enterTransition = inflateTransition(toEnterTransitionId)
//                                    toFragment.sharedElementEnterTransition = inflateTransition(transitionInfo.sharedElementEnterTransitionId!!)
//                                }
//                            }
//                            beginTransaction()
//                                    .replace(R.id.container, toFragment, route.to)
//                                    .let {
//                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                                            it.addSharedElement(commonView, this@MainActivity.getString(transitionInfo.nameId!!))
//                                        } else {
//                                            it
//                                        }
//                                    }
//                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                                    .let {
//                                        if (route.addToBackStack) it.addToBackStack(null) else it
//                                    }.commit()
//                        }
//                    }
//                } else {