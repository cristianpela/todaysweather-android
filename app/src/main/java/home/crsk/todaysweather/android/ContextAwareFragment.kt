package home.crsk.todaysweather.android

import android.support.v4.app.Fragment
import org.koin.standalone.KoinComponent

/**
 * .
 * Created by Cristian Pela on 16.02.2018.
 */
abstract class ContextAwareFragment() : Fragment(), KoinComponent {

    abstract val contextName: String

}