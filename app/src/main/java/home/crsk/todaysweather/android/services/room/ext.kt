package home.crsk.todaysweather.android.services.room

import home.crsk.todaysweather.android.dataroom.location.LocationDTO
import home.crsk.todaysweather.android.dataroom.search.SearchHistoryDTO
import home.crsk.todaysweather.domain.common.entity.Location

/**
 * .
 * Created by Cristian Pela on 5/1/2018.
 */

fun String.wildcardStartWith() = "$this%"
fun String.wildcardEndsWith() = "%$this"

fun SearchHistoryDTO.toDomainEntity(): String = this.search

fun String.toDatabaseEntity(): SearchHistoryDTO = SearchHistoryDTO(this)

fun Location.toDatabaseEntity(): LocationDTO = LocationDTO().apply {
    city = this@toDatabaseEntity.city
    countryCode = this@toDatabaseEntity.countryCode
    locationKey = this@toDatabaseEntity.locationKey
    latitude = this@toDatabaseEntity.latitude
    longitude = this@toDatabaseEntity.longitude
    isFavorite = this@toDatabaseEntity.favorite
    recordUpdateTime = System.currentTimeMillis()
}

fun LocationDTO.toDomainEntity(): Location = Location(city, countryCode, locationKey, latitude, longitude, isFavorite)