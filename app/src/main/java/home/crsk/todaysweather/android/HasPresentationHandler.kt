package home.crsk.todaysweather.android

import android.os.Bundle
import home.crsk.todaysweather.android.presentation.HasPresentation
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.State
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import plusAssign
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 19/12/2017.
 */
class HasPresentationHandler<out S : State> {

    private val laterSavingInstance: Bundle = Bundle()

    private val arguments: Bundle = Bundle()

    private var intentSender = PublishSubject.create<Intent>()

    private var areStreamsSubscribed = false

    private val pendingIntents = mutableListOf<Intent>()

    private val intentSenderInterface = object : IntentSender {
        override fun sendIntent(intent: Intent) = intentSender.onNext(intent)
    }

    private var disposables = CompositeDisposable()

    private val handlerPostAssistant = HandlerPostAssistant()

    fun allowRendererToPrepareViews(intentRenderer: IntentRenderer<S>): Unit = intentRenderer.onAllowViewsToPrepare()// we make sure we have the views prepared before subscribing to them events

    fun subscribeStreams(hasPresentation: HasPresentation<*>, intentRenderer: IntentRenderer<S>) {

        Loggy.d("HasPresentationHandler subscribe render streams for $intentRenderer")

        disposables += hasPresentation.presentation.observeState().subscribe { state ->
            @Suppress("UNCHECKED_CAST")
            intentRenderer.renderState(state as S)
        }

        disposables += hasPresentation.presentation.observeMessages().subscribe { message ->
            intentRenderer.renderMessages(message)
        }

        disposables += hasPresentation.presentation.observeErrors().subscribe { err ->
            intentRenderer.renderErrors(err)
        }


        handlerPostAssistant.post({
            Loggy.d("HasPresentationHandler subscribe intent streams for $intentRenderer")
            disposables += hasPresentation.presentation
                    .bindIntents(Observable.merge(
                            listOf(intentSender) + intentRenderer.provideIntentStreams()))
        })

        handlerPostAssistant.post({
            areStreamsSubscribed = true
            intentRenderer.onAllowSendIntent(intentSenderInterface, getLaterSavingInstance())
            intentRenderer.onArgumentsPassed(arguments)
            //send pending intents
            pendingIntents.forEach { intentSender.onNext(it) }
            pendingIntents.clear()
        })
    }

    fun disposeStreams(intentRenderer: IntentRenderer<S>) {
        Loggy.d("HasPresentationHandler dispose streams for $intentRenderer")
        // preemptive send a cancel intent, in case the domain presentation want's to process canceling
        // and thus avoiding leaking subscriptions from hot observables or subjects
        intentSender.onNext(Intent.CancelIntent)
        areStreamsSubscribed = false
        pendingIntents.clear()
        handlerPostAssistant.clearAllActions()
        disposables.clear()

    }


    fun updateBundles(savedInstanceState: Bundle?, args: Bundle?) {
        savedInstanceState?.let { laterSavingInstance.putAll(it) }
        args?.let { arguments.putAll(it) }
    }

    fun getLaterSavingInstance(): Bundle = Bundle(laterSavingInstance)

    fun addToLaterSavingInstance(key: String, value: Any?) =
            when (value) {
                is String -> laterSavingInstance.putString(key, value)
                is Int -> laterSavingInstance.putInt(key, value)
                is Float -> laterSavingInstance.putFloat(key, value)
                is Double -> laterSavingInstance.putDouble(key, value)
                else -> throw UnsupportedOperationException("Adding ${value?.javaClass} currently not supported")
            }


    /**
     * assure that any intent that is trying to be send before onAllowSendIntent will be
     * qeued up in pendingIntens
     */
    fun sendIntentMaybePending(intent: Intent) {
        if (areStreamsSubscribed) {
            intentSender.onNext(intent)
        } else {
            pendingIntents.add(intent)
        }
    }

    fun removeFromLaterFromSavingInstance(key: String) = laterSavingInstance.remove(key)

    fun post(action: () -> Unit) = handlerPostAssistant.post(action)

    fun postDelayed(value: Long = 300, unit: TimeUnit = TimeUnit.MILLISECONDS, action: () -> Unit): Long =
            handlerPostAssistant.postDelayed(value, unit, action)

    fun clearPostAction(token: Long) = handlerPostAssistant.clearAction(token)

    fun clearAllPostActions() = handlerPostAssistant.clearAllActions()


}

interface IntentSender {
    fun sendIntent(intent: Intent)
}