package home.crsk.todaysweather.android.services.location

import android.location.Criteria
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Bundle
import home.crsk.todaysweather.android.stripDiacritics
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.LocationDetector
import home.crsk.todaysweather.domain.common.gateway.LocationNotDetectedError
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 *
 * Created by Cristian Pela on 7/12/2017.
 */
class LocationDetectorImpl(private val rxGeocoder: RxGeocoder,
                           private val locationManager: LocationManager) : LocationDetector {

    override fun detect(useLastKnownLocation: Boolean): Observable<Location> =
            Observable
                    .create<DeviceLocation> { emitter: ObservableEmitter<DeviceLocation> ->
                        val bestProvider = bestProvider()
                        val lastLocation = locationManager.getLastKnownLocation(bestProvider)
                        if (lastLocation != null && lastLocation.isValid()) {
                            emit(lastLocation, emitter)
                        } else {
                            val listener = object : LocationListener by trait {
                                override fun onLocationChanged(newLoc: DeviceLocation?) = newLoc?.let {
                                    emit(it, emitter)
                                } ?: emitter.onError(LocationNotDetectedError(Error(), LanguageProvider.ERR_LOCATION_NO_PLACE_DECODED))

                                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                                    if (status == LocationProvider.OUT_OF_SERVICE ||
                                            status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
                                        emitter.onError(LocationNotDetectedError(Error(), LanguageProvider.ERR_LOCATION_PROVIDER_UNAVAILABLE))
                                    }
                                }
                            }
                            locationManager.requestLocationUpdates(bestProvider, 0L, 0.0f, listener)
                            emitter.setCancellable {
                                locationManager.removeUpdates(listener)
                            }
                        }
                    }
                    .timeout(2, TimeUnit.MINUTES)
                    .flatMap { deviceLocation ->
                        rxGeocoder
                                .getAddressFromLocation(deviceLocation.latitude, deviceLocation.longitude)
                                .map { address ->
                                    Location(address.locality, address.countryCode, latitude = address.latitude,
                                            longitude = address.longitude)
                                }
                    }
                    .onErrorResumeNext { err: Throwable ->
                        when (err) {
                            is TimeoutException -> Observable
                                    .error<Location>(LocationNotDetectedError(
                                            Error(err), LanguageProvider.ERR_LOCATION_PROVIDER_UNAVAILABLE))
                            else -> Observable.error(err)
                        }
                    }

    private fun bestProvider(): String = LocationManager.PASSIVE_PROVIDER
//            locationManager.getBestProvider(Criteria().apply {
//                accuracy = Criteria.ACCURACY_LOW
//                powerRequirement = Criteria.POWER_MEDIUM
//                isAltitudeRequired = false
//                isBearingRequired = false
//            }, true)

    private fun DeviceLocation.isValid(): Boolean = latitude > 0.0 || longitude > 0.0

    private fun emit(currentLocation: DeviceLocation, emitter: ObservableEmitter<DeviceLocation>) {
        emitter.onNext(currentLocation)
        emitter.onComplete()
    }

    private val trait = object : LocationListener {
        override fun onLocationChanged(p0: DeviceLocation?) = Unit

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) = Unit

        override fun onProviderEnabled(p0: String?) = Unit

        override fun onProviderDisabled(p0: String?) = Unit
    }
}

typealias DeviceLocation = android.location.Location

