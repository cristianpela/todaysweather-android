package home.crsk.todaysweather.android.services.resources

import android.content.Context
import android.support.v4.content.ContextCompat
import home.crsk.todaysweather.android.R
import home.crsk.todaysweather.domain.common.gateway.ResourceFinder
import java.lang.ref.WeakReference


/**
 * .
 * Created by Cristian Pela on 11/12/2017.
 */
class ResourceFinderImpl(context: Context) : ResourceFinder {

    private val weakContext: WeakReference<Context> = WeakReference(context)

    override fun findArray(name: String): Array<String> =
            getId<R.array>(name)?.let {
                weakContext.get()?.resources?.getStringArray(it) ?: emptyArray()
            } ?: emptyArray()

    override fun findColor(name: String): Int =
            getId<R.color>(name)?.let { id ->
                weakContext.get()?.let { context -> ContextCompat.getColor(context, id) } ?: -1
            } ?: -1

    override fun findInt(name: String): Int =
            getId<R.integer>(name)?.let { weakContext.get()?.resources?.getInteger(it) ?: -1 } ?: -1

    override fun findString(name: String, vararg params: Any): String =
            getId<R.string>(name)?.let { weakContext.get()?.getString(it, params) ?: name } ?: name

    override fun findDrawableId(name: String): Int {
        val id = getId<R.drawable>(name)
        return if (id == null) -1 else id
    }

    private inline fun <reified T> getId(name: String): Int? =
            try {
                T::class.java.getField(name).getInt(null)
            } catch (e: Exception) {
                null
            }
}