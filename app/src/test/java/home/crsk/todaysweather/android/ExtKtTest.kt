package home.crsk.todaysweather.android

import org.junit.Test

import org.junit.Assert.*

/**
 * .
 * Created by Cristian Pela on 30/12/2017.
 */
class ExtKtTest {
    @Test
    fun removeDiacritics() {
        assertEquals("AIST", "ĂÎȘȚ".stripDiacritics())
    }

    @Test
    fun extractFileNameFromPath() {
        assertEquals("chanceflurries", "https://icons.wxug.com/i/c/a/chanceflurries.gif"
                .extractFileNameFromPath())

        assertEquals("chanceflurries.gif", "https://icons.wxug.com/i/c/a/chanceflurries.gif"
                .extractFileNameFromPath(true))

        assertEquals("chanceflurries", "https://icons.wxug.com/i/c/a/chanceflurries"
                .extractFileNameFromPath(true))

        assertEquals(null, "abc"
                .extractFileNameFromPath(true))

        assertEquals(null, "abc"
                .extractFileNameFromPath())
    }

}