package home.crsk.todaysweather.android

import org.junit.After
import org.junit.Test
import org.koin.dsl.module.applicationContext
import org.koin.standalone.StandAloneContext.closeKoin
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.test.KoinTest
import org.koin.test.dryRun

/**
 * .
 * Created by Cristian Pela on 24/12/2017.
 */
class CyclingDepKoinTest : KoinTest {

    @After
    fun after() {
        closeKoin()
    }

    @Test
    fun dryRunTest() {
        startKoin(TodaysWeatherApplication.DI().modules.subList(0, 2))
        dryRun()
    }

}
